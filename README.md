Coverage of the [provided board tests][tests]: ![coverage][cov]

[tests]: https://gitlab.gwdg.de/app/flowerwarspp/blob/master/specification/test-usage.md
[cov]: https://gitlab.gwdg.de/hans-georg.sommer/floragnomewars/badges/master/coverage.svg

# About the game

See the [original README](specification/README.md) file.


# Quickstart

Assuming you have downloaded the release archive and opened a terminal in the directory
containing the archive, the following commands extract the archive and build and start the game:

```shell
unzip FloraGnomeWars.zip
cd FloraGnomeWars
ant
java -jar FloraGnomeWars.jar
```

# How to build

## Dependencies
- `java` >= version 8.0
- `ant` >= 1.8.0 - for building the project
- `checkstyle` - for source code checks

## Build the jar file

Run `ant` in the root directory to build the jar file.

## Build the documentation

Run `ant doc` in the root directory, the resulting .html files are located in `./doc`.

## Run the source code checks

Execute `ant checkstyle` in the root directory of the repository. The configuration for the checks
is located in [checkstyle/style.xml](checkstyle/style.xml).


# How to run

Run the game by executing `java -jar FloraGnomeWars.jar`. For possible command line parameters see
below. There are no required parameters, if the program is run without arguments, a single local
game with two interactive players on a board with size 11 is started.

## Game play

The controls should be mostly self-explanatory. Flowers and ditches can be set by left-clicking,
the first flower of a move can be reset by right-clicking anywhere. The menu allows to restart the
game or tournament at any time.

## Command line arguments

The following command line arguments can be used to customize the game:

- `-h`, `--help`

    Show a help message and exit.

- `-s <N>`, `--size <N>`

    Set the size of the game board. Values in the range 3 ≤ N ≤ 30 are allowed. Default: 11

- `-1 <type>`, `--player1 <type>`

    Player type of the first player. This player will be the red player in a single game or start
    as red in a tournament. The possible values are: `advanced`, `human`, `random`, `remote`, `simple`.
    Default: `human`.

- `-2 <type>`, `--player2 <type>`

    Player type of the second player. This player will be the blue player in a single game or start
    as blue in a tournament. The possible values are the same as above. Default: `human`.

    Note: In the current implementation it's not possible to set both players to `remote`.

- `-H <host>`, `--host <host>`
- `-P <port>`, `--port <port>`
- `-N <name>`, `--name <name>`

    These three parameters set the connection configuration for network games. If a player is
    offered, `--host` should be set either to `localhost` (the default) or a public IP address of
    the computer the game is running on. The other two parameters can be changed or leaved at their
    default values (port `2342`, name `FGW`).

    If you want to connect to a offered player not running on the same machine, the `--host`
    parameter has to be set to the IP address or resolvable host name of the remote host. The `port`
    and `name` have to match the remote settings, so the default values should work if the same
    implementation of the game is running on both sides.

- `-o <type>`, `--offer <type>`

    Offer a player of the given type to a remote game. Possible values are the same as for the
    players, except remote.

- `-t <N>`, `--tournament <N>`

    Play the given number of rounds with alternating player colors.

- `--delay <N>`

    Delay for player moves in milli seconds. Default: 1000

- `--disable-bg`

    Disable the swapping of the background color to match the current players color. Useful for
    AI games and tournaments with low or zero delay to reduce annoying screen flickering.

- `--debug`

    Enable debug mode.

- `--headless`

    Start the game without GUI. This setting disables the delay and the program exits, once the
    game or tournament is finished (this does not apply to offered network games). This mode may be
    useful for games running on a headless server or to run the game from a script (e. g. to play
    multiple tournaments with AI players on changing board sizes).


# Known issues

## Network games on Debian-based distributions

If you use a Debian-based Linux distribution (this includes Ubuntu and Linux Mint) and offer a
player on the network, the game instance trying to connect to this player may encounter the
following error: `ConnectException: Connection refused to host: 127.0.1.1`. This is caused by the
fact, that Debian by default maps the host name to this address in the file `/etc/hosts`. To resolve
this issue, you may either delete or comment that line, or (e.g. if you don't have root access)
provide the `-H` or `--host` parameter with your IP address. To identify your IP address, execute
`ip a` or `ifconfig`.
