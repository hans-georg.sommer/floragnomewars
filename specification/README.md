# Intro
Die Gebrüder Gartenpfleger Torsten und Torben streiten sich um Tamara und möchten sie durch das Herrichten einer hübschen Gartenlandschaft
beeindrucken. Leider müssen sich die beiden die Wiese hierfür teilen. Wer schafft es, die meisten Gärten zu bepflanzen und diese zu
verzieren, indem sie durch Gräben miteinander verbunden werden?

# FlowerWars
- [Spielregeln](rules.md)
- [Hilfestellungen zur Implementierung](implementation-tips.md)
- [Computerspieler Strategie](simple-strategy.md)
- [Verwendung der automatischen Tests](test-usage.md)
- [Verwendung der Spielbrettanzeige](display-usage.md)
- [Diverses...](stuff.md)

![Spielvorschau](images/flowerwarspp-preview.png)

# Herkunft
FlowerWarsPP ist eine Variation des Spiels [Ponte del Diavolo](https://www.brettspielnetz.de/spielregeln/ponte+del+diavolo.php).
