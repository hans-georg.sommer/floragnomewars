package visualtree;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

import flowerwarspp.preset.Move;

import java.util.Collection;

public class RedBlueTreePanel extends JPanel {
    private static final long serialVersionUID = 1L;
    DrawableTreeElement node;
  
    public RedBlueTreePanel() {
        setDoubleBuffered(true);
        this.node = null;
    }
  
    public void draw(DrawableTreeElement root) {
        this.node = root;
        repaint();
    }
  
    public void paint(Graphics gOld) {
        Graphics2D g = (Graphics2D)gOld;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    
        drawTree(g);
    }
  
    private void drawTree(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
    
        int depth = getDepth(this.node);
        Dimension d = getPreferredSize();
        int width = (int)d.getWidth();
        int height = (int)d.getHeight();
    
        int size = 50;
        while (size > Math.pow(0.5D, depth - 1) * width) {
            size--;
        }
        int steps = depth > 4 ? depth : 4;
    
        drawSubTree(g, this.node, 0, width, height / (steps + 1), height / (steps + 1), size);
    }
  
    private void drawSubTree(Graphics2D g, DrawableTreeElement node, int xMin, int xMax, int yLayer, int yLayerStep, int size) {
        int x = xMin + (xMax - xMin) / 2;
        int y = yLayer;
        if (node != null) {
            int numberOfChildren = node.getChildren().size();
            int y1 = y + yLayerStep;

            int[] x1 = new int[numberOfChildren];

            for (int i = 0; i < numberOfChildren; i++) {
                x1[i] = xMin + (xMax - xMin) * (2 * i + 1) / (2 * numberOfChildren);
            }
            
            for (int i = 0; i < numberOfChildren; i++) {
                g.setColor(Color.BLACK);
                g.setStroke(new BasicStroke(2.0F));
                g.drawLine(x, y, x1[i], y1);
            }          

            int counter = 0;
            for (DrawableTreeElement n : node.getChildren()) {
                drawSubTree(g, n, xMin + counter * (xMax - xMin) / numberOfChildren, xMin + (counter + 1) * (xMax - xMin) / numberOfChildren, yLayer + yLayerStep, yLayerStep, size);
                counter++;
            }        
        }

        drawNode(g, x, y, size, node != null ? node.getValue() : null, node != null ? node.isRed() : false);
    }
  
    private int getDepth(DrawableTreeElement node) {
        if (node == null) {
            return 1;
        }

        int depth = 0;
        for (DrawableTreeElement n : node.getChildren()) {
            int cur = getDepth(n);
            if (depth < cur) {
                depth = cur;
            }
        }
        return 1 + depth;
    }
  
    private void drawNode(Graphics2D g, int x, int y, int size, Move value, boolean red) {
        boolean leaf = value == null;
    
        int fontSize = size / 3;
        if (leaf) {
            g.setColor(Color.BLACK);
            g.fillRect(x - size / 2, y - 2 * size / 6, size, 2 * size / 3);
        } else {
            g.setColor(red ? Color.RED : Color.BLUE);
            g.fillOval(x - size / 2, y - size / 2, size, size);
      
            g.setColor(Color.BLACK);
            g.drawOval(x - size / 2, y - size / 2, size, size);
        }
        String text = value != null ? value.toString() : "null";
        Font font;
        int textWidth;
        int textHeight;
        do {
            font = new Font("Dialog", 1, fontSize);
            FontMetrics metrics = g.getFontMetrics(font);
            textWidth = metrics.stringWidth(text);
            textHeight = metrics.getAscent();
            fontSize--;
            } while (textWidth > 0.8D * size);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(text, x - textWidth / 2, y + textHeight / 2);
    }
}
