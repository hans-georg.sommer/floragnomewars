package visualtree;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;

public class RedBlueTreeDrawer extends JFrame {
    private static final long serialVersionUID = 1L;
    private RedBlueTreePanel panel;
    private boolean build;
  
    public RedBlueTreeDrawer(boolean build) {
        this.build = build;
        if (!build) {
            return;
        }

        this.panel = new RedBlueTreePanel();
        this.panel.setPreferredSize(new Dimension(800, 600));
        add(this.panel);
    
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                Container content = RedBlueTreeDrawer.this.getContentPane();
                RedBlueTreeDrawer.this.panel.setPreferredSize(new Dimension(content.getWidth(), content.getHeight()));
                RedBlueTreeDrawer.this.panel.repaint();
            }
        });
        pack();
    
        setTitle("RedBlueTreeDrawer - adapted by Felix Spuehler");
        setDefaultCloseOperation(3);
        setVisible(true);
    }

    public void draw(DrawableTreeElement root) {
        if (!build) {
            return;
        }
        this.panel.draw(root);
    }
}