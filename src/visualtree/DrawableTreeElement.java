package visualtree;

import java.util.Collection;

import flowerwarspp.preset.Move;

public interface DrawableTreeElement {
    public Collection<DrawableTreeElement> getChildren();
    
    public boolean isRed ();
    
    public Move getValue ();
}