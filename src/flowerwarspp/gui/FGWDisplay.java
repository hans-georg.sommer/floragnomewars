package flowerwarspp.gui;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.border.EmptyBorder;

/**
 * Main window of the games graphical interface.
 *
 * @author Hans-Georg Sommer
 */
public class FGWDisplay extends JFrame {
    /** Panel for the game content. */
    private final BoardPanel panel = new BoardPanel(this);

    /** Status bar label. */
    private JLabel statusLabel = new JLabel("");

    /**
     * Create a new display.
     */
    public FGWDisplay() {
        super("FloraGnomeWars Alpha 3.14 - Molehill");
        add(panel);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.setSize(getContentPane().getWidth(), getContentPane().getHeight());
            }
        });
        GameMenu menu = new GameMenu(this);
        menu.add(Box.createHorizontalGlue());
        menu.add(statusLabel);
        statusLabel.setBorder(new EmptyBorder(0, 0, 0, 6));
        setJMenuBar(menu);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(320, 240));
        setSize(800, 600);
        setLocationRelativeTo(null); // center on screen
        setVisible(true);
    }

    /**
     * Get a reference to the menu of the display.
     *
     * @return The menu bar.
     */
    public JMenuBar getMenu() {
        return getJMenuBar();
    }

    /**
     * Get a reference to the board display.
     *
     * @return The board display.
     */
    public BoardDisplay getDisplay() {
        return panel;
    }

    /**
     * Enable or disable background color swapping;
     *
     * @param enabled
     *        Enable background color swapping if {@code true}.
     */
    public void setBackgroundEnabled(boolean enabled) {
        panel.setBackgroundEnabled(enabled);
    }

    /**
     * Update the status bar.
     *
     * @param str
     *        New status text.
     */
    public void updateStatusBar(String str) {
        statusLabel.setText(str);
    }
}
