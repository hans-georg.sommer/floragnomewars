package flowerwarspp.gui;

import flowerwarspp.preset.Move;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.Viewer;

/**
 * Interface for the output of the game, for example on a graphical interface.
 *
 * @author Hans-Georg Sommer
 */
public interface BoardDisplay {
    /**
     * Set a new viewer.
     *
     * The display uses this object to retrieve information about the current
     * game situation.
     *
     * @param viewer
     *        The new viewer.
     */
    public void setViewer(Viewer viewer);

    /**
     * Set a handler for input events.
     *
     * @param input
     *        The new input handler.
     */
    public void setInputHandler(MoveRequesterGui input);

    /**
     * Set the player names so that they can be displayed.
     *
     * @param first
     *        First player name.
     * @param second
     *        Second player name.
     */
    public void setPlayerNames(String first, String second);

    /**
     * Set the current round in tournament to odd or even;
     *
     * If {@code true} (odd), the first player is red, otherwise blue.
     *
     * @param isOdd
     *        If {@code true}, the current round is odd.
     */
    public void setOddRound(boolean isOdd);

    /**
     * Reset current move and status and redraw.
     */
    public void reset();

    /**
     * Set latest move and current status and redraw.
     *
     * @param move
     *        The latest move.
     * @param status
     *        The current status of the board.
     */
    public void update(Move move, Status status);
}
