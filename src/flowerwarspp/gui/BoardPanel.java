package flowerwarspp.gui;

import flowerwarspp.Debug;
import flowerwarspp.preset.Ditch;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveType;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Position;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.Viewer;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Panel containing the actual game content (board, score display etc.).
 *
 * @author Hans-Georg Sommer
 */
public class BoardPanel extends JPanel implements BoardDisplay {
    /*
     * All scale factor values are relative to the edge length of a single
     * field/triangle on the board (tileWidth member variable).
     */
    /** Scale factor for the width of ditches. */
    private static final float DITCH_SCALE = 0.07F;
    /** Scale factor for the board grid. */
    private static final float GRID_SCALE = 0.1F;
    /** Scale factor for the grid nodes. */
    private static final float NODE_SCALE = 0.3F;

    /** Color for the red player. */
    private static final Color COL_RED = new Color(124, 45, 51);
    /** Color for the blue player. */
    private static final Color COL_BLUE = new Color(43, 43, 93);
    /** Color for new elements of the red player. */
    private static final Color COL_RED_NEW = new Color(216, 60, 50);
    /** Color for new elements of the blue player. */
    private static final Color COL_BLUE_NEW = new Color(85, 85, 255);
    /** Used as background color if {@code backgroundEnabled == false}. */
    private static final Color COL_BG = new Color(151, 72, 153);
    /** Grid color. */
    private static final Color GRID_COL = new Color(80, 80, 80);
    /** Font color. */
    private static final Color FONT_COL = new Color(255, 255, 255);
    /** Font color for result overlay. */
    private static final Color RESULT_COL = new Color(197, 177, 42);

    /** Move object for the end move. */
    private static final Move END_MOVE = new Move(MoveType.End);

    /** Parent frame object. */
    private final FGWDisplay parent;
    /** Handler for input events. */
    private MoveRequesterGui input;
    /** Button to surrender. */
    private final JButton btnGiveUp = new JButton("Give up");
    /** Button to end the game. */
    private final JButton btnEnd = new JButton("End game");
    /** Board viewer. */
    private Viewer viewer;
    /** Current width of the panel. */
    private int panelWidth;
    /** Current height of the panel. */
    private int panelHeight;
    /** Edge length of a single field on the board. */
    private float tileWidth;
    /** Maps grid coordinates to screen coordinates as values. */
    private Map<Position, Point2D> nodes;
    /** Maps polygons (screen coordinates) to corresponding flower objects. */
    private Map<Polygon, Flower> flowers;
    /** Maps polygons (screen coordinates) to corresponding ditch objects. */
    private Map<Polygon, Ditch> ditches;

    /** Enable/disable background in current player color. */
    private boolean backgroundEnabled = true;
    /** Current board status. */
    private Status currentStatus;
    /** Last move. */
    private Move lastMove;
    /** Color of the current player. */
    private PlayerColor currentTurn = PlayerColor.Red;
    /** Name to display for the first player. */
    private String firstName = "first";
    /** Name to display for the second player. */
    private String secondName = "second";
    /** Tracks if the current tournament round is odd (first player is red). */
    private boolean isOddRound = true;

    /**
     * Create a new board panel.
     *
     * @param display
     *        The parent JFrame.
     */
    public BoardPanel(FGWDisplay display) {
        parent = display;
        setLayout(null);
        setDoubleBuffered(true);
        btnGiveUp.setToolTipText("Surrender");
        btnGiveUp.setFocusPainted(false);
        btnGiveUp.setEnabled(false);
        add(btnGiveUp);
        btnEnd.setToolTipText("End the game now");
        btnEnd.setFocusPainted(false);
        btnEnd.setEnabled(false);
        add(btnEnd);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if (currentStatus != null && currentStatus != Status.Ok) {
                    currentStatus = null;
                    repaint();
                    return;
                }
                // The panel doesn't have a reference to the GameLoop and
                // doesn't need it elsewhere, so lets abuse the menu here
                if (currentStatus == null && viewer.getStatus() != Status.Ok
                        && parent.getMenu().getMenu(0).getItem(0).isEnabled()
                        && JOptionPane.showConfirmDialog(null, "Do you want to start a new game?",
                                "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    parent.getMenu().getMenu(0).getItem(0).doClick();
                    repaint();
                    return;
                }
                if (input == null) {
                    return;
                }
                if (SwingUtilities.isRightMouseButton(event)) {
                    synchronized (input) {
                        if (input.hasFlower()) {
                            input.setFlower(null);
                            repaint();
                        }
                    }
                    return;
                }
                Point point = event.getPoint();
                Ditch ditch = toDitch(point);
                if (ditch != null) {
                    if (viewer.getPossibleDitches(viewer.getTurn()).contains(ditch)) {
                        synchronized (input) {
                            input.setMove(new Move(ditch));
                            input.notify();
                        }
                    }
                    return;
                }
                Flower flower = toFlower(point);
                if (flower != null && viewer.getPossibleFlowers(viewer.getTurn()).contains(flower)) {
                    synchronized (input) {
                        if (input.hasFlower()) {
                            Move move = new Move(input.getFlower(), flower);
                            if (viewer.getPossibleMoves().contains(move)) {
                                input.setMove(move);
                                input.notify();
                                return;
                            }
                        }
                        input.setFlower(flower);
                        repaint();
                    }
                }
            }
        });
    }

    /*
     * @see flowerwarspp.gui.BoardDisplay#reset()
     */
    @Override
    public synchronized void reset() {
        lastMove = null;
        currentStatus = null;
        currentTurn = viewer.getTurn();
        if (input != null) {
            btnGiveUp.setEnabled(true);
            synchronized (input) {
                input.setFlower(null);
            }
        }
        repaint();
    }

    /**
     * Set a new input handler.
     *
     * @param requester
     *        Object which handles the mouse inputs.
     */
    @Override
    public synchronized void setInputHandler(MoveRequesterGui requester) {
        input = requester;
        btnGiveUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                synchronized (input) {
                    input.setMove(new Move(MoveType.Surrender));
                    input.notify();
                }
            }
        });
        btnGiveUp.setEnabled(true);
        btnEnd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                synchronized (input) {
                    input.setMove(END_MOVE);
                    input.notify();
                }
            }
        });
    }

    /*
     * @see flowerwarspp.gui.BoardDisplay#setViewer(flowerwarspp.preset.Viewer)
     */
    @Override
    public synchronized void setViewer(Viewer viewer) {
        this.viewer = viewer;
        recalcPositions();
    }

    /**
     * Enable or disable background color swapping;
     * @param enabled Enable background color swapping if {@code true}.
     */
    public void setBackgroundEnabled(boolean enabled) {
        backgroundEnabled = enabled;
    }

    /*
     * @see flowerwarspp.gui.BoardDisplay#setPlayerNames(java.lang.String, java.lang.String)
     */
    @Override
    public void setPlayerNames(String first, String second) {
        firstName = (first != null) ? first : "";
        secondName = (second != null) ? second : "";
    }

    /*
     * @see flowerwarspp.gui.BoardDisplay#setOddRound(boolean)
     */
    @Override
    public void setOddRound(boolean isOdd) {
        isOddRound = isOdd;
    }

    /**
     * Resize the panel and adjust the positions of all elements.
     *
     * @param width
     *        New width.
     * @param height
     *        New height.
     * @see java.awt.Component#setSize(int, int)
     */
    @Override
    public void setSize(int width, int height) {
        panelWidth = width;
        panelHeight = height;
        recalcPositions();
    }

    /*
     * @see flowerwarspp.gui.BoardDisplay#update(flowerwarspp.preset.Move, flowerwarspp.preset.Status)
     */
    @Override
    public synchronized void update(Move move, Status status) {
        Debug.print("Update display: " + move + " " + status);
        lastMove = move;
        currentStatus = status;
        currentTurn = viewer.getTurn();
        if (input != null) {
            if (currentStatus != Status.Ok) {
                btnGiveUp.setEnabled(false);
                btnEnd.setEnabled(false);
            } else {
                if (viewer.getPossibleMoves().contains(END_MOVE)) {
                    btnEnd.setEnabled(true);
                } else {
                    btnEnd.setEnabled(false);
                }
            }
        }
        repaint();
    }

    /**
     * Recalculate the screen positions of all elements.
     */
    private synchronized void recalcPositions() {
        if (viewer == null) {
            Debug.print("BoardDrawer.recalcPositions(): viewer is null");
            return;
        }
        int size = viewer.getSize();
        tileWidth = (float) Math.min(panelWidth / size,
                panelHeight / size / Math.sin(Math.toRadians(60))) * 0.9F;
        double tileHeight = Math.sin(Math.toRadians(60)) * tileWidth;
        double minX = (panelWidth - size * tileWidth) / 2;
        double maxY = (panelHeight + size * tileHeight) / 2;

        nodes = new HashMap<Position, Point2D>();
        for (int j = 0; j <= size; j++) {
            for (int k = 0; k <= size - j; k++) {
                Point2D.Double point = new Point2D.Double(minX + (k + j * 0.5) * tileWidth,
                                                          maxY - j * tileHeight);
                nodes.put(new Position(k + 1, j + 1), point);
            }
        }

        flowers = new HashMap<Polygon, Flower>();
        for (int j = 1; j <= size; j++) {
            for (int k = 1; k <= size; k++) {
                if (j + k <= size + 1) {
                    Flower f = new Flower(new Position(j, k),
                                          new Position(j, k + 1),
                                          new Position(j + 1, k));
                    flowers.put(toPolygon(f), f);
                }
                if (j + k <= size) {
                    Flower f = new Flower(new Position(j + 1, k + 1),
                                          new Position(j, k + 1),
                                          new Position(j + 1, k));
                    flowers.put(toPolygon(f), f);
                }
            }
        }

        ditches = new HashMap<Polygon, Ditch>();
        for (int j = 1; j <= size + 1; j++) {
            for (int k = 1; k <= size; k++) {
                if (j + k <= size + 1) {
                    Ditch ditch1 = new Ditch(new Position(j, k), new Position(j, k + 1));
                    Ditch ditch2 = new Ditch(new Position(j, k), new Position(j + 1, k));
                    ditches.put(toPolygon(ditch1), ditch1);
                    ditches.put(toPolygon(ditch2), ditch2);
                }
                if ((j > 1) && (j + k <= size + 2)) {
                    Ditch ditch3 = new Ditch(new Position(j, k), new Position(j - 1, k + 1));
                    ditches.put(toPolygon(ditch3), ditch3);
                }
            }
        }

        int step = panelWidth / 100;
        int width = step * 16;
        int height = step * 3;
        int padding = step * 2;
        btnGiveUp.setBounds(padding, padding, width, height);
        btnEnd.setBounds(padding, height + padding * 2, width, height);
    }

    /**
     * Create a polygon in screen coordinates from the given flower.
     *
     * @param flower
     *        The flower used.
     * @return The resulting polygon.
     */
    private Polygon toPolygon(Flower flower) {
        Point2D p1 = nodes.get(flower.getFirst());
        Point2D p2 = nodes.get(flower.getSecond());
        Point2D p3 = nodes.get(flower.getThird());
        int[] xpoints = {(int) p1.getX(), (int) p2.getX(), (int) p3.getX()};
        int[] ypoints = {(int) p1.getY(), (int) p2.getY(), (int) p3.getY()};
        return new Polygon(xpoints, ypoints, 3);
    }

    /**
     * Get the flower object located at the specified point on the screen.
     *
     * @param point
     *        The input point on the screen.
     * @return The corresponding flower or null if no flower was found.
     */
    private synchronized Flower toFlower(Point point) {
        for (Map.Entry<Polygon, Flower> elem : flowers.entrySet()) {
            if (elem.getKey().contains(point)) {
                return elem.getValue();
            }
        }
        return null;
    }

    /**
     * Create a polygon in screen coordinates from the given ditch.
     *
     * @param ditch
     *        The ditch used.
     * @return The resulting polygon.
     */
    private Polygon toPolygon(Ditch ditch) {
        Position first = ditch.getFirst();
        Position second = ditch.getSecond();
        // initialize angle for horizontal ditch
        double angle = 90;
        if (first.getColumn() == second.getColumn()) {
            // orientation bottom left - top right
            angle = 30;
        } else if (first.getRow() != second.getRow()) {
            // orientation top left - bottom right
            angle = 150;
        }
        double dx = tileWidth * DITCH_SCALE * Math.cos(Math.toRadians(angle));
        double dy = tileWidth * DITCH_SCALE * Math.sin(Math.toRadians(angle));
        Point2D from = nodes.get(first);
        Point2D to = nodes.get(second);
        int[] xpoints = new int[] {(int) (from.getX() + dx), (int) (from.getX() - dx),
                                   (int) (to.getX() - dx), (int) (to.getX() + dx)};
        int[] ypoints = new int[] {(int) (from.getY() + dy), (int) (from.getY() - dy),
                                   (int) (to.getY() - dy), (int) (to.getY() + dy)};
        return new Polygon(xpoints, ypoints, 4);
    }

    /**
     * Get the ditch object located at the specified point on the screen.
     *
     * @param point
     *        The input point on the screen.
     * @return The corresponding flower or null if no ditch was found.
     */
    private Ditch toDitch(Point point) {
        for (Map.Entry<Polygon, Ditch> elem : ditches.entrySet()) {
            if (elem.getKey().contains(point)) {
                return elem.getValue();
            }
        }
        return null;
    }

    /**
     * Draw the panel content.
     *
     * @param g
     *        The graphics object to draw on.
     */
    private synchronized void draw(Graphics2D g) {
        if (viewer == null) {
            Debug.print("BoardDrawer.draw(): viewer is null");
            return;
        }
        if (backgroundEnabled) {
            g.setColor(currentTurn == PlayerColor.Red ? COL_RED_NEW : COL_BLUE_NEW);
        } else {
            g.setColor(COL_BG);
        }
        g.fill(new Rectangle2D.Float(0, 0, panelWidth, panelHeight));
        /* draw board background */
        int maxIndex = viewer.getSize() + 1;
        Point2D p1 = nodes.get(new Position(1, 1));
        Point2D p2 = nodes.get(new Position(maxIndex, 1));
        Point2D p3 = nodes.get(new Position(1, maxIndex));
        g.setColor(Color.GRAY);
        g.fill(new Polygon(new int[] {(int) p1.getX(), (int) p2.getX(), (int) p3.getX()},
                           new int[] {(int) p1.getY(), (int) p2.getY(), (int) p3.getY()}, 3));

        /* draw flowers */
        g.setColor(COL_RED);
        for (Flower flower : viewer.getFlowers(PlayerColor.Red)) {
            g.fill(toPolygon(flower));
        }
        g.setColor(COL_BLUE);
        for (Flower flower : viewer.getFlowers(PlayerColor.Blue)) {
            g.fill(toPolygon(flower));
        }
        g.setColor(Color.WHITE);
        for (Flower flower : viewer.getPossibleFlowers(viewer.getTurn())) {
            g.fill(toPolygon(flower));
        }
        if (input != null && input.hasFlower()) {
            g.setColor(viewer.getTurn() == PlayerColor.Red ? COL_RED_NEW : COL_BLUE_NEW);
            g.fill(toPolygon(input.getFlower()));
        }

        /* draw grid */
        g.setStroke(new BasicStroke(tileWidth * GRID_SCALE));
        g.setColor(GRID_COL);
        for (int i = 1; i <= maxIndex; ++i) {
            g.draw(new Line2D.Double(nodes.get(new Position(i, 1)),
                                     nodes.get(new Position(1, i))));
            g.draw(new Line2D.Double(nodes.get(new Position(i, 1)),
                                     nodes.get(new Position(i, maxIndex - i + 1))));
            g.draw(new Line2D.Double(nodes.get(new Position(1, i)),
                                     nodes.get(new Position(maxIndex - i + 1, i))));
        }

        /* draw ditches */
        g.setStroke(new BasicStroke(tileWidth * DITCH_SCALE));
        g.setColor(COL_RED);
        for (Ditch ditch : viewer.getDitches(PlayerColor.Red)) {
            g.draw(new Line2D.Double(nodes.get(ditch.getFirst()), nodes.get(ditch.getSecond())));
        }
        g.setColor(COL_BLUE);
        for (Ditch ditch : viewer.getDitches(PlayerColor.Blue)) {
            g.draw(new Line2D.Double(nodes.get(ditch.getFirst()), nodes.get(ditch.getSecond())));
        }
        g.setColor(Color.WHITE);
        for (Ditch ditch : viewer.getPossibleDitches(viewer.getTurn())) {
            g.draw(new Line2D.Double(nodes.get(ditch.getFirst()), nodes.get(ditch.getSecond())));
        }

        /* highlight latest move */
        if (lastMove != null) {
            g.setColor(currentTurn == PlayerColor.Blue ? COL_RED_NEW : COL_BLUE_NEW);
            if (lastMove.getType() == MoveType.Flower) {
                g.setStroke(new BasicStroke(tileWidth * GRID_SCALE));
                g.draw(toPolygon(lastMove.getFirstFlower()));
                g.draw(toPolygon(lastMove.getSecondFlower()));
            } else if (lastMove.getType() == MoveType.Ditch) {
                g.setStroke(new BasicStroke(tileWidth * DITCH_SCALE * 0.6F));
                g.draw(toPolygon(lastMove.getDitch()));
            }
        }

        /* draw nodes */
        g.setColor(GRID_COL);
        double d = tileWidth * NODE_SCALE * (Debug.isEnabled() ? 1 : 0.7);
        for (Point2D point : nodes.values()) {
            g.fill(new Ellipse2D.Double(point.getX() - d / 2, point.getY() - d / 2, d, d));
        }
        if (Debug.isEnabled()) {
            g.setFont(g.getFont().deriveFont(1, tileWidth * 0.1F));
            FontMetrics metrics = g.getFontMetrics();
            g.setColor(FONT_COL);
            for (Map.Entry<Position, Point2D> entry : nodes.entrySet()) {
                Point2D point = entry.getValue();
                Position pos = entry.getKey();
                String str = String.format("%d,%d", pos.getColumn(), pos.getRow());
                if (point != null) {
                    float width = (float) (point.getX() - metrics.stringWidth(str) / 2);
                    float height = (float) (point.getY() + metrics.getAscent() / 2);
                    g.drawString(str, width, height);
                }
            }
        }

        /* draw scores */
        Font font = g.getFont();
        float base = panelWidth / 10;
        float x = panelWidth - base * 3.2F;
        float width = base * 1.4F;
        float padding = base * 0.2F;
        int pointsRed = viewer.getPoints(PlayerColor.Red);
        int pointsBlue = viewer.getPoints(PlayerColor.Blue);
        int pointsFirst = isOddRound ? pointsRed : pointsBlue;
        int pointsSecond = isOddRound ? pointsBlue : pointsRed;

        drawScoreBox(g, x, padding, width, base,
                Color.WHITE, GRID_COL, isOddRound ? COL_RED : COL_BLUE,
                font.deriveFont(pointsFirst > pointsSecond ? Font.BOLD : Font.PLAIN, base * 0.5F),
                font.deriveFont(base * 0.2F), "" + pointsFirst, firstName);
        drawScoreBox(g, x + padding + width, padding, width, base,
                Color.WHITE, GRID_COL, isOddRound ? COL_BLUE : COL_RED,
                font.deriveFont(pointsSecond > pointsFirst ? Font.BOLD : Font.PLAIN, base * 0.5F),
                font.deriveFont(base * 0.2F), "" + pointsSecond, secondName);

        /* draw overlay */
        if (currentStatus != null && currentStatus != Status.Ok) {
            drawResult(g, font.deriveFont(Font.BOLD, base * 1.5F));
        }
    }

    /**
     * Draw a rounded rectangle containing points and name of a player.
     *
     * @param g
     *        Graphics object to draw on.
     * @param x
     *        X value of the top left corner of the box.
     * @param y
     *        Y value of the top left corner of the box.
     * @param width
     *        Width of the box.
     * @param height
     *        Height of the box.
     * @param fontColor
     *        Color for the font.
     * @param borderColor
     *        Color for the border of the box.
     * @param fillColor
     *        Fill color for the box.
     * @param pointFont
     *        Font to be used for the points.
     * @param nameFont
     *        Font to be used for the player name.
     * @param points
     *        String to draw for the points.
     * @param name
     *        String to draw for the player name.
     */
    private void drawScoreBox(Graphics2D g, float x, float y, float width, float height,
                              Color fontColor, Color borderColor, Color fillColor,
                              Font pointFont, Font nameFont, String points, String name) {
        Rectangle2D.Double roundRect = new Rectangle2D.Double(x, y, width, height);
        g.setColor(fillColor);
        g.fill(roundRect);
        g.setColor(borderColor);
        g.setStroke(new BasicStroke(Math.min(width, height) * 0.03F));
        g.draw(roundRect);
        g.setColor(fontColor);
        g.setFont(pointFont);
        FontMetrics metrics = g.getFontMetrics();
        float oneThird = height / 3;
        g.drawString(points, x + (width - metrics.stringWidth(points)) / 2,
                y + oneThird + metrics.getAscent() / 2);
        g.setFont(nameFont);
        metrics = g.getFontMetrics();
        g.drawString(name, x + (width - metrics.stringWidth(name)) / 2,
                y + height - (oneThird - metrics.getAscent()) / 2);
    }

    /**
     * Draw the game result on an semi-transparent overlay.
     *
     * @param g
     *        The graphics object on which to draw.
     * @param font
     *        The font to use.
     */
    private void drawResult(Graphics2D g, Font font) {
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8F));
        g.setColor(Color.BLACK);
        g.fill(new Rectangle2D.Float(0, 0, panelWidth, panelHeight));
        g.setFont(font);
        g.setColor(RESULT_COL);
        String str = "";
        switch (currentStatus) {
        case RedWin:
            str = "Red wins!";
            break;
        case BlueWin:
            str = "Blue wins!";
            break;
        case Draw:
            str = "Draw!";
            break;
        case Illegal:
            str = "Illegal state!!!";
            break;
        default:
            throw new IllegalStateException("Unknown state!");
        }
        FontMetrics metrics = g.getFontMetrics();
        g.drawString(str, (panelWidth - metrics.stringWidth(str)) / 2,
                          (panelHeight + metrics.getAscent()) / 2);
    }

    /*
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        draw(g);
    }
}
