package flowerwarspp.argparse;

/**
 * Contains all possible argument types.
 *
 * @author Hans-Georg Sommer
 */
public enum ArgumentType {
    /**
     * For boolean-like on/off options. Arguments of this type are always
     * optional and don't expect a value.
     */
    TYPE_FLAG,
    /**
     * For arguments of type Integer.
     */
    TYPE_INT,
    /**
     * For String type arguments. Use this if no more specific type is
     * available.
     */
    TYPE_STRING
}
