package flowerwarspp.argparse;

import flowerwarspp.preset.ArgumentParserException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Class representing command line parameters.
 *
 * @author Hans-Georg Sommer
 */
public class Argument {
    /**
     * Name of the argument.
     */
    private final String name;
    /**
     * Short (for example UNIX-like single letter) argument form.
     */
    private final String shortName;
    /**
     * Type of the argument.
     */
    private final ArgumentType type;
    /**
     * Description used in help output.
     */
    private String help;
    /**
     * Set of allowed values. Currently only recognized if of type
     * {@link ArgumentType#TYPE_STRING}.
     */
    private SortedSet<String> choices;
    /**
     * Default value. If set, the argument is optional.
     */
    private Object defaultValue;

    /**
     * Construct an Argument with the given type and names.
     *
     * @param name
     *        Argument name.
     * @param shortName
     *        Short argument name.
     * @param type
     *        Argument type.
     */
    public Argument(String name, String shortName, ArgumentType type) {
        this.name = name;
        this.shortName = shortName;
        this.type = type;
        help = "";
    }

    /**
     * Set the allowed values for this argument. Only possible for
     * {@link ArgumentType#TYPE_STRING} arguments.
     *
     * @param choices
     *        Allowed argument values.
     * @return {@code this}, to allow for builder pattern style method chaining.
     * @throws ArgumentParserException
     *         If {@code this} is not of type {@link ArgumentType#TYPE_STRING}.
     */
    public Argument setChoices(Collection<String> choices) throws ArgumentParserException {
        if (type != ArgumentType.TYPE_STRING) {
            throw new ArgumentParserException("Wrong argument type, can not assign choices");
        }
        this.choices = new TreeSet<String>(choices);
        return this;
    }

    /**
     * Set a default argument value for an Integer argument.
     *
     * @param value
     *        New default value.
     * @return {@code this}, to allow for builder pattern style method chaining.
     * @throws ArgumentParserException
     *         If {@code this} is not of type {@link ArgumentType#TYPE_INT}.
     */
    public Argument setDefault(int value) throws ArgumentParserException {
        if (type != ArgumentType.TYPE_INT) {
            throw new ArgumentParserException("Wrong default value type for parameter: " + name);
        }
        defaultValue = value;
        return this;
    }

    /**
     * Set a default argument value for a String argument.
     *
     * @param value
     *        New default value.
     * @return {@code this}, to allow for builder pattern style method chaining.
     * @throws ArgumentParserException
     *         If {@code this} is not of type {@link ArgumentType#TYPE_STRING}.
     */
    public Argument setDefault(String value) throws ArgumentParserException {
        if (type != ArgumentType.TYPE_STRING) {
            throw new ArgumentParserException("Wrong default value type for parameter: " + name);
        }
        defaultValue = value;
        return this;
    }

    /**
     * Set a description for use in help output.
     *
     * @param help
     *        Description of the argument.
     * @return {@code this}, to allow for builder pattern style method chaining.
     */
    public Argument setHelp(String help) {
        this.help = help;
        return this;
    }

    /**
     * Getter for the {@link ArgumentType}.
     *
     * @return ArgumentType of the argument.
     */
    public ArgumentType getType() {
        return type;
    }

    /**
     * Get the values allowed for this argument.
     *
     * @return Set of the allowed values.
     */
    public SortedSet<String> getChoices() {
        return choices;
    }

    /**
     * Get the default value of this argument.
     *
     * @return The default value.
     */
    public Object getDefault() {
        return defaultValue;
    }

    /**
     * Check, if this argument is optional. Optional arguments are either of
     * type {@link ArgumentType#TYPE_FLAG} or have a default value set.
     *
     * @return {@code true}, if this argument is optional, {@code false}
     *         otherwise.
     */
    public boolean isOptional() {
        return (type == ArgumentType.TYPE_FLAG || defaultValue != null);
    }

    /**
     * Build a String representation used in program usage help output. Optional
     * arguments are enclosed in brackets.
     *
     * @return String representation using the {@link Argument#shortName}, if
     *         available.
     */
    public String toUsageString() {
        String param = (shortName == null ? "--" + name : "-" + shortName);
        String value = "";
        if (type == ArgumentType.TYPE_INT) {
            value = " INT";
        } else if (type == ArgumentType.TYPE_STRING) {
            value = " STRING";
        }
        if (isOptional()) {
            return String.format("[%s%s]", param, value);
        }
        return String.format("%s%s", param, value);
    }

    /**
     * Print help output for this argument. Includes both short and long form,
     * default and allowed values, if set.
     */
    public void printHelp() {
        System.out.printf("%6s--%-12s%s\n", (shortName == null ? "" : "-" + shortName + ", "),
                name, help);
        if (choices != null) {
            System.out.printf("%20spossible values: %s\n", "", choices);
        }
        if (defaultValue != null) {
            System.out.printf("%20sdefault: %s\n", "", defaultValue);
        }
    }
}
