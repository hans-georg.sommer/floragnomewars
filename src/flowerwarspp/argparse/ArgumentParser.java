package flowerwarspp.argparse;

import flowerwarspp.preset.ArgumentParserException;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * A Command line parser which is inspired by Python's {@code argparse} module,
 * but implements only a small subset of it's features.
 *
 * @author Hans-Georg Sommer
 */
public class ArgumentParser {
    /**
     * Contains all arguments for this parser.
     */
    private final Map<String, Argument> params;
    /**
     * Mapping from short to long argument names.
     */
    private final Map<String, String> shortNames;
    /**
     * Stores the actual options given on the command line
     */
    private Map<String, Object> options;
    /**
     * Program name (or something alike), used in usage output.
     */
    private final String prog;
    /**
     * Program description used in help output.
     */
    private final String description;

    /**
     * Create a new parser for {@code prog} with given description.
     *
     * @param prog
     *        Value for {@code prog}.
     * @param description
     *        Value for {@code description}.
     */
    public ArgumentParser(String prog, String description) {
        params = new TreeMap<>();
        shortNames = new TreeMap<>();
        this.prog = prog;
        this.description = description;
    }

    /**
     * Add an argument to the parser.
     *
     * @param name
     *        Long form name for the new argument.
     * @param shortName
     *        Short form name for the argument.
     * @param type
     *        {@link ArgumentType} of the argument.
     * @return The new argument, to allow for builder pattern style method
     *         chaining.
     */
    public Argument addArgument(String name, String shortName, ArgumentType type) {
        if (shortName != null) {
            shortNames.put(shortName, name);
        }
        Argument arg = new Argument(name, shortName, type);
        params.put(name, arg);
        return arg;
    }

    /**
     * Add an argument to the parser. This overload does not set a short name.
     *
     * @param name
     *        Long form name for the new argument.
     * @param type
     *        {@link ArgumentType} of the argument.
     * @return The new argument, to allow for builder pattern style method
     *         chaining.
     */
    public Argument addArgument(String name, ArgumentType type) {
        return addArgument(name, null, type);
    }

    /**
     * Parse command line arguments according to the provided arguments.
     *
     * @param args
     *        Array of strings to parse.
     * @throws ArgumentParserException
     *         If
     *         <ul>
     *         <li>An unknown parameter is found.</li>
     *         <li>The same argument occurs multiple times (ignored for flag
     *         arguments).</li>
     *         <li>The value for an {@link ArgumentType#TYPE_INT} argument can
     *         not be parsed as Integer.</li>
     *         <li>The value for an {@link ArgumentType#TYPE_STRING} argument is
     *         not in the allowed values.</li>
     *         <li>The end of the command line arguments was reached, but
     *         another value was expected.</li>
     *         <li>A required argument wasn't found.</li>
     *         </ul>
     */
    public void parseArgs(final String[] args) throws ArgumentParserException {
        options = new TreeMap<>();
        for (int i = 0; i < args.length; ++i) {
            String arg = args[i];
            if (arg.equals("-h") || arg.equals("--help")) {
                printHelp();
            }
            if (arg.startsWith("--")) {
                arg = arg.substring(2);
            } else if (arg.startsWith("-")) {
                if (shortNames.containsKey(arg.substring(1))) {
                    arg = shortNames.get(arg.substring(1));
                } else {
                    arg = null;
                }
            }
            if (arg == null || !params.containsKey(arg)) {
                throw new ArgumentParserException("Unknown parameter: " + args[i]);
            }
            Argument param = params.get(arg);
            if (options.containsKey(arg) && param.getType() != ArgumentType.TYPE_FLAG) {
                throw new ArgumentParserException("multiple occurences of parameter " + args[i]);
            }
            switch (param.getType()) {
            case TYPE_FLAG:
                options.put(arg, true);
                break;
            case TYPE_INT:
                try {
                    options.put(arg, Integer.parseInt(args[i + 1]));
                    ++i;
                } catch (NumberFormatException e) {
                    throw new ArgumentParserException("Wrong format for parameter: " + args[i]);
                } catch (IndexOutOfBoundsException e) {
                    throw new ArgumentParserException("Missing value for parameter: " + args[i]);
                }
                break;
            case TYPE_STRING:
                String value;
                try {
                    value = args[i + 1];
                } catch (IndexOutOfBoundsException e) {
                    throw new ArgumentParserException("Missing value for parameter: " + args[i]);
                }
                if (param.getChoices() != null && !param.getChoices().contains(value)) {
                    throw new ArgumentParserException(value
                            + " is not a valid value for parameter " + args[i]);
                }
                options.put(arg, value);
                ++i;
                break;
            default:
                throw new ArgumentParserException(args[i] + " is of unknown parameter type");
            }
        }

        // look for missing arguments and insert default values
        for (Map.Entry<String, Argument> entry : params.entrySet()) {
            String name = entry.getKey();
            Argument arg = entry.getValue();
            // Note: can not use arg.isOptional() here
            if (arg.getType() != ArgumentType.TYPE_FLAG) {
                if (!options.containsKey(name)) {
                    if (arg.getDefault() != null) {
                        options.put(name, arg.getDefault());
                    } else {
                        throw new ArgumentParserException("Parameter " + name + " is required");
                    }
                }
            }
        }
    }

    /**
     * Check if a request for an argument value is valid.
     *
     * @param name
     *        Name of the requested Argument.
     * @throws ArgumentParserException
     *         If {@code parseArgs()} wasn't called before or the requested
     *         value is not present.
     */
    private void checkIfValidRequest(String name) throws ArgumentParserException {
        if (options == null) {
            throw new ArgumentParserException("Argument requested before calling parseArgs()");
        }
    }

    /**
     * Request the value for an Integer argument.
     *
     * @param name
     *        Name of the requested argument.
     * @return Value of the requested argument.
     * @throws ArgumentParserException
     *         If wrong argument type.
     */
    public int getInt(String name) throws ArgumentParserException {
        checkIfValidRequest(name);
        if (params.get(name).getType() != ArgumentType.TYPE_INT) {
            throw new ArgumentParserException(name + " is not of Integer type");
        }
        return (Integer) options.get(name);
    }

    /**
     * Request the value for a String argument.
     *
     * @param name
     *        Name of the requested argument.
     * @return Value of the requested argument.
     * @throws ArgumentParserException
     *         If wrong argument type.
     */
    public String getString(String name) throws ArgumentParserException {
        checkIfValidRequest(name);
        if (params.get(name).getType() != ArgumentType.TYPE_STRING) {
            throw new ArgumentParserException(name + " is not of String type");
        }
        return (String) options.get(name);
    }

    /**
     * Request, whether a flag was set.
     *
     * @param name
     *        Name of the requested flag.
     * @return {@code true}, if the flag was set.
     * @throws ArgumentParserException
     *         If wrong argument type.
     */
    public boolean getFlag(String name) throws ArgumentParserException {
        checkIfValidRequest(name);
        if (params.get(name).getType() != ArgumentType.TYPE_FLAG) {
            throw new ArgumentParserException(name + " is not of Flag type");
        }
        return options.containsKey(name);
    }

    /**
     * Print a short usage information for the program.
     */
    public void printUsage() {
        TreeSet<String> args = new TreeSet<>();
        for (Argument arg : params.values()) {
            args.add(arg.toUsageString());
        }
        System.out.printf("Usage: %s [-h] %s\n", prog, String.join(" ", args));
    }

    /**
     * Print a description of the program and all command line arguments.
     *
     * Exit successfully afterwards.
     */
    private void printHelp() {
        printUsage();
        addArgument("help", "h", ArgumentType.TYPE_FLAG)
                .setHelp("show this help message and exit");
        System.out.printf("\n%s\n", description);
        System.out.println("\nRequired arguments:");
        for (Argument arg : params.values()) {
            if (!arg.isOptional()) {
                arg.printHelp();
            }
        }
        System.out.println("\nOptional arguments:");
        for (Argument arg : params.values()) {
            if (arg.isOptional()) {
                arg.printHelp();
            }
        }
        System.exit(0);
    }
}
