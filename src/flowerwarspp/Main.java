package flowerwarspp;

import flowerwarspp.argparse.ArgumentParser;
import flowerwarspp.argparse.ArgumentType;
import flowerwarspp.boardmechanics.FGWBoard;
import flowerwarspp.gui.BoardDisplay;
import flowerwarspp.gui.FGWDisplay;
import flowerwarspp.gui.GameMenu;
import flowerwarspp.gui.MoveRequesterGui;
import flowerwarspp.network.FGWNet;
import flowerwarspp.player.BasicPlayer;
import flowerwarspp.player.HumanPlayer;
import flowerwarspp.player.IntelligentPlayer;
import flowerwarspp.player.RandomPlayer;
import flowerwarspp.player.SimplePlayer;
import flowerwarspp.player.NetworkPlayer;
import flowerwarspp.preset.ArgumentParserException;
import flowerwarspp.preset.Player;
import flowerwarspp.preset.Requestable;
import flowerwarspp.preset.Viewer;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Main class that starts the game.
 *
 * @author Hans-Georg Sommer
 */
public class Main {
    /** Input object from which moves can be requested by interactive players. */
    private static Requestable input;
    /** Stores the provided configuration for network games. */
    private static FGWNet network;
    /** Name for the registration and lookup of remote players. */
    private static String rmiName;

    /**
     * Create a new player of the given type
     *
     * @param type
     *        Player type.
     * @return The created player.
     */
    private static Player createPlayer(String type) {
        switch (type) {
        case "human":
            HumanPlayer player = new HumanPlayer();
            player.setInputSource(input);
            return player;
        case "remote":
            return network.find(rmiName);
        case "simple":
            return new SimplePlayer();
        case "advanced":
            return new IntelligentPlayer();
        default:
            return new RandomPlayer();
        }
    }

    /**
     * The main function which starts the game.
     *
     * @param args
     *        Array of command line arguments.
     *
     * @throws ArgumentParserException
     *         If the ArgumentParser is not used correctly.
     * @throws RemoteException
     *         On network errors.
     */
    private static void startGame(String[] args) throws ArgumentParserException, RemoteException {
        ArrayList<String> playerTypes = new ArrayList<>(Arrays.asList("human", "random", "remote", "simple", "advanced"));
        ArgumentParser parser = new ArgumentParser("java -jar FloraGnomeWars.jar",
                "An implementation of the FlowerWarsPP game.");
        parser.addArgument("size", "s", ArgumentType.TYPE_INT)
                .setDefault(11)
                .setHelp("the size of the game board");
        parser.addArgument("player1", "1", ArgumentType.TYPE_STRING)
                .setChoices(playerTypes)
                .setDefault("human")
                .setHelp("player type of the first player (starts as red)");
        parser.addArgument("player2", "2", ArgumentType.TYPE_STRING)
                .setChoices(playerTypes)
                .setDefault("human")
                .setHelp("player type of the second player (starts as blue)");
        playerTypes.remove("remote");
        parser.addArgument("offer", "o", ArgumentType.TYPE_STRING)
                .setChoices(playerTypes)
                .setDefault("none")
                .setHelp("offer a player of the given type to a remote game");
        parser.addArgument("host", "H", ArgumentType.TYPE_STRING)
                .setDefault("localhost")
                .setHelp("host for remote connection");
        parser.addArgument("port", "P", ArgumentType.TYPE_INT)
                .setDefault(2342)
                .setHelp("port for remote connection");
        parser.addArgument("name", "N", ArgumentType.TYPE_STRING)
                .setDefault("FGW")
                .setHelp("name for remote connection");
        parser.addArgument("tournament", "t", ArgumentType.TYPE_INT)
                .setDefault(1)
                .setHelp("play the given number of rounds");
        parser.addArgument("delay", ArgumentType.TYPE_INT)
                .setDefault(1000)
                .setHelp("delay for player moves in milli seconds");
        parser.addArgument("debug", ArgumentType.TYPE_FLAG)
                .setHelp("print debug output");
        parser.addArgument("disable-bg", ArgumentType.TYPE_FLAG)
                .setHelp("disable background color swapping");
        parser.addArgument("headless", ArgumentType.TYPE_FLAG)
                .setHelp("start the game without GUI, disable the delay, exit when finished");
        parser.parseArgs(args);

        int size = parser.getInt("size");
        String player1 = parser.getString("player1");
        String player2 = parser.getString("player2");
        int rounds = parser.getInt("tournament");
        network = new FGWNet(parser.getString("host"), parser.getInt("port"));
        rmiName = parser.getString("name");
        int delay = parser.getInt("delay");
        Debug.setEnabled(parser.getFlag("debug"));

        if (size < 3 || size > 30) {
            throw new ArgumentParserException("Invalid board size: Only values 3 ≤ size ≤ 30 are allowed.");
        }

        FGWBoard board = new FGWBoard(size);
        Viewer viewer = board.viewer();
        FGWDisplay gui = null;
        BoardDisplay display = null;
        if (!parser.getFlag("headless")) {
            gui = new FGWDisplay();
            gui.setBackgroundEnabled(!parser.getFlag("disable-bg"));
            display = gui.getDisplay();
            display.setViewer(viewer);
        }
        if (player1.equals("human") || player2.equals("human")) {
            if (parser.getFlag("headless")) {
                input = new MoveRequesterCli(viewer);
            } else {
                input = new MoveRequesterGui();
                display.setInputHandler((MoveRequesterGui) input);
            }
        }

        if (!parser.getString("offer").equals("none")) {
            // offer player to the network
            Player localPlayer = createPlayer(parser.getString("offer"));
            ((BasicPlayer) localPlayer).setDisplay(display);
            network.offer(new NetworkPlayer(localPlayer), rmiName);
        } else {
            // game runs on this instance
            if (player1.equals("remote") && player2.equals("remote")) {
                throw new ArgumentParserException("Two remote players are not supported.");
            }
            Player playerRed = createPlayer(player1);
            Player playerBlue = createPlayer(player2);
            GameLoop loop = new GameLoop(board, playerRed, playerBlue, rounds, delay, gui);
            if (gui != null) {
                display.setPlayerNames(player1, player2);
                ((GameMenu) gui.getMenu()).activate(loop);
            }
            loop.start();
        }
    }

    /**
     * Entry point of the program.
     *
     * All errors which may be occur here end the game, so there is no reason to
     * handle them locally.
     *
     * @param args
     *        Array of command line arguments.
     */
    public static void main(String[] args) {
        try {
            startGame(args);
        } catch (ArgumentParserException | RemoteException e) {
            System.out.println("Error: " + e.getMessage());
            System.exit(1);
        }
    }
}
