package flowerwarspp.boardmechanics;

import java.util.Collection;
import java.util.HashMap;
import java.util.ArrayList;

import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Ditch;

/**
 * This is the class for generating possible ditch moves, generating the corresponding increment and making this move.
 * @author Felix Spuehler
 */
public class PossibleMoveDitch implements PossibleMove {
    /** Reference to the board. */
    private FGWBoard board;
    /** Map of possible ditches. */
    private HashMap<Integer, ColoredDitch> possibleDitches;
    /** Color of associated player. */
    private FGWType color;
    /** Corresponding move-increment-map. */
    private HashMap<Move, FGWIncrement> increments;

    /**
     * Constructor which fetches the possible ditches of the player color.
     * @param board Reference to the board.
     */
    public PossibleMoveDitch(FGWBoard board) {
        this.board = board;
        increments = new HashMap<Move, FGWIncrement>();

        if (board.getCurrentPlayerColor() ==  PlayerColor.Red) {
            color = FGWType.RED;
            possibleDitches = board.getRedPossibleDitches();
        } else {
            color = FGWType.BLUE;
            possibleDitches = board.getBluePossibleDitches();
        }
    }

    /**
     * Generate all possible ditch moves.
     * @return All possible ditchmoves as an ArrayList.
     */
    @Override
    public Collection<Move> generateMoves() {
        ArrayList<Move> possibleMoves = new ArrayList<Move>();

        for (ColoredDitch d : possibleDitches.values()) {
            possibleMoves.add(new Move(d));
        }

        return possibleMoves;
    }

    /**
     * Makes a move using the increment.
     * @param move A ditch move.
     */
    @Override
    public void make(Move move) {
        if (increments.containsKey(move)) {
            increments.get(move).make();
        } else {
            ColoredDitch ditch = board.getColoredDitch(move.getDitch());
            FGWIncrement increment = new FGWIncrement(board);
            increment.addToDitches(ditch, color);
            increment.excludeTheRedPossibleFlower(ditch.getFlowerEdgeNeighbors());
            increment.excludeTheBluePossibleFlower(ditch.getFlowerEdgeNeighbors());
            increment.setDitchToFallow(ditch.getDitchNeighbors());
            increment.make();
            increments.put(move, increment);
        }
    }

    /**
     * Undoes the move using the increment.
     */
    @Override
    public void unmake(Move move) {
        increments.get(move).unmake();
    }
}
