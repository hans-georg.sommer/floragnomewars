package flowerwarspp.boardmechanics;

import flowerwarspp.preset.Viewer;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.Ditch;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.PlayerColor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Viewer for the FGWBoard.
 * @author Charlotte Ackva, Felix Spuehler, Martin Heide
 **/
public class FGWBoardViewer implements Viewer {
    /** Reference to the board. */
    private FGWBoard board;

    /** Constructs a viewer for the specified board.
     * @param board The board the viewer is for.
     */
    public FGWBoardViewer(FGWBoard board) {
        super();
        this.board = board;
    }

    @Override
    public PlayerColor getTurn() {
        return board.getCurrentPlayerColor();
    }

    @Override
    public int getSize() {
        return board.getBoardSize();
    }

    @Override
    public Status getStatus() {
        return board.getCurrentStatus();
    }

    @Override
    public Collection<Flower> getFlowers(final PlayerColor color) {
        return Collections.unmodifiableCollection(board.getFlowers(color));
    }

    @Override
    public Collection<Flower> getPossibleFlowers(final PlayerColor color) {
        if (color == PlayerColor.Red) {
            return new ArrayList<Flower>(board.getRedPossibleFlowers().values());
        } else {
            return new ArrayList<Flower>(board.getBluePossibleFlowers().values());
        }
    }

    @Override
    public Collection<Ditch> getDitches(final PlayerColor color) {
        return Collections.unmodifiableCollection(board.getDitches(color));
    }

    @Override
    public Collection<Ditch> getPossibleDitches(final PlayerColor color) {
        if (color == PlayerColor.Red) {
            return new ArrayList<Ditch>(board.getRedPossibleDitches().values());
        } else {
            return new ArrayList<Ditch>(board.getBluePossibleDitches().values());
        }
    }

    @Override
    public Collection<Move> getPossibleMoves() {
        return board.getPossibleMoves();
    }

    @Override
    public int getPoints(final PlayerColor color) {
        return board.getPoints(color);
    }

    @Override
    public Collection<Move> getPossibleMovesFlower() {
        return board.getPossibleMovesFlower();
    }

    @Override
    public Collection<Move> getPossibleMovesDitch() {
        return board.getPossibleMovesDitch();
    }
}
