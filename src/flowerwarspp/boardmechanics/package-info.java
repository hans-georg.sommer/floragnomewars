/**
 * Contains everything for the board mechanics and game rules, e.g. the board, the viewer, extended ditch and flower classes...
 */
package flowerwarspp.boardmechanics;