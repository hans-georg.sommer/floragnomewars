package flowerwarspp.boardmechanics;

import java.util.Collection;
import flowerwarspp.preset.Move;

/**
 * An interface for the the PossibleMoveFlower and PossibleMoveDitch classes.
 * @version 1.0
 * @author Felix Spuehler
 */
public interface PossibleMove {

    /**
     * Generates all possible moves.
     * @return All possible moves.
     */
    public abstract Collection<Move> generateMoves();

    /**
     * Makes a move using increments.
     * @param move A move.
     */
    public abstract void make (final Move move);

    /**
     * Undoes a move using increments.
     * @param move A move.
     */
    public abstract void unmake(final Move move);
}
