package flowerwarspp.boardmechanics;

import java.util.HashMap;

import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveType;

import flowerwarspp.boardmechanics.PossibleMove;
import flowerwarspp.boardmechanics.PossibleMoveFlower;
import flowerwarspp.boardmechanics.PossibleMoveDitch;

/**
 * In order to calculate some game moves in advance, the AI has to go back and forth several moves. <br>
 * The objects of this class are used to store these moves on a stack, together with its corresponding state on the board. <br>
 * An object of this class holds the move, the possible move object and the possible moves for the corresponding state. <br> <br>
 * If you want to use long stacks with many possible moves you will get a memory error (e.g. if you build big trees directly on a 30-board).
 * @author Felix Spuehler, Charlotte Ackva
 * @version 1.0
 */
public class FGWStackObject {
    /** The move to store. */
    private final Move move;
    /** The MoveType of the move. */
    private final MoveType type;
    /** The corresponding move object (contains the increment). */
    private PossibleMove possibleMoveObject;
    /** The possible flower moves at this state of the board. */
    private HashMap<Move, PossibleMoveFlower> possibleMovesFlower;
    /**Tthe possible ditch moves at this state of the board. */
    private HashMap<Move, PossibleMoveDitch> possibleMovesDitch;


    /** Constructs a FGWStackObject for a move and its corresponding possible moves lists.
     * @param move The move.
     * @param possibleMovesFlower HashMap of possible flower moves with PossibleMoveFlower object.
     * @param possibleMovesDitch HashMap of possible ditch moves with PossibleMoveDitch object.
     */
    protected FGWStackObject(Move move, HashMap<Move, PossibleMoveFlower> possibleMovesFlower, HashMap<Move, PossibleMoveDitch> possibleMovesDitch) {
        this.move = move;
        type = move.getType();
        this.possibleMovesFlower = new HashMap<Move, PossibleMoveFlower>();
        this.possibleMovesDitch = new HashMap<Move, PossibleMoveDitch>();
        if (type == MoveType.Flower) {
            possibleMoveObject = possibleMovesFlower.get(move);
        } else {
            possibleMoveObject = possibleMovesDitch.get(move);
        }
    }

    /**
     * Returns the move.
     * @return The Move.
     */
    protected Move getMove() {
        return move;
    }

    /**
     * Returns the PossibleMoveFlower / PossibleMoveDitch object corresponding to the move.
     * @return The PossibleMove object.
     */
    protected PossibleMove getPossibleMoveObject() {
        return possibleMoveObject;
    }

    /**
     * Returns the HashMap of possible flower moves.
     * @return HashMap of moves with PossibleMove object.
     */
    protected HashMap<Move,PossibleMoveFlower> getPossibleMovesFlower() {
        return possibleMovesFlower;
    }

    /**
     * Returns the HashMap of possible ditch moves.
     * @return HashMap of moves with PossibleMove object.
     */
    protected HashMap<Move,PossibleMoveDitch> getPossibleMovesDitch() {
        return possibleMovesDitch;
    }
}
