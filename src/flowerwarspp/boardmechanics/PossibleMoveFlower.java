package flowerwarspp.boardmechanics;

import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Flower;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This is a class for generating and making flower moves. A flower move consists
 * in planting two flowers. For a fixed first flower, this class provides methods to generate
 * all possible flower moves given this first flower (i.e. this method returns
 * a collection of flower tupels), and to make a particular move. Within these methods,
 * the possible flowers and ditches are updated due to the game rules
 * and these changes are stored in increments. When a move is made, the changes
 * stored in the increment are applied to the board.
 * @version 1.0
 * @author Charlotte Ackva, (Martin Heide, Felix Spuehler)
 */
public class PossibleMoveFlower implements PossibleMove {
    /** The color of the current player. */
    private FGWType color;
    /** The board where we work on. */
    private FGWBoard board;
    /** An increment to store the changes after planting the first flower. */
    private FGWIncrement firstIncrement;
    /** An increment to store the changes after planting the second flower. */
    private HashMap<Flower, FGWIncrement> secondIncrements;

    /** All possible flowers of the player color on the board (before setting a move). */
    private HashMap<Integer, ColoredFlower> possibleFlowers;

    /** First flower to be planted. */
    private ColoredFlower firstFlower;
    /** Second flower to be planted. */
    private ColoredFlower secondFlower;

    /** The FlowerSet which is updated/created after planting a flower. */
    private FlowerSet flowerSetToUpdate;
    /** The FlowerSets which have to be set to impossible after planting a flower. */
    private Collection<FlowerSet> flowerSetsToDelete;

    /**
     * Constructs a PossibleMoveFlower object.
     * Depending on the player color, the list of possible flowers is fetched from
     * the board and the first flower is set to impossible for this color.
     * Then the neighborhood of this first flower is checked and updated (see below for further details.)
     * @param board The board where we're working on.
     * @param flower The first flower of a flower move tupel.
     */
    public PossibleMoveFlower (final FGWBoard board, ColoredFlower flower) {
        this.board = board;
        firstIncrement = new FGWIncrement(board);
        firstFlower = flower;
        flowerSetsToDelete = new HashSet<FlowerSet>();
        secondIncrements = new HashMap<Flower, FGWIncrement>();

        if (board.getCurrentPlayerColor() == PlayerColor.Red) {
            color = FGWType.RED;
            firstIncrement.excludeTheRedPossibleFlower(flower);
            possibleFlowers = board.getRedPossibleFlowers();
        } else {
            color = FGWType.BLUE;
            firstIncrement.excludeTheBluePossibleFlower(flower);
            possibleFlowers = board.getBluePossibleFlowers();
        }

        firstFlower.setFlowerType(color);
        assignFlowerToFlowerSet(firstFlower, firstIncrement);
        checkFlowerNeighborhood(firstFlower, firstIncrement);
        firstFlower.setFlowerType(FGWType.UNCOLORED);
    }

    /**
     * Returns the size of the parent FlowerSet, if existing. Else, returns size 1 ("FlowerSet" of one flower).
     * @param flower The flower to ckeck.
     * @return Size of the parent FlowerSet.
     */
    public int getParentFlowerSetSize(ColoredFlower flower) {
        if (flower.getParentFlowerSet() != null) {
            return flower.getParentFlowerSet().getSize();
        } else {
            return 1;
        }
    }

    /**
     * Returns the flower edge neighbors of the parent FlowerSet, if existing.
     * Else, returns the flower edge neighbors of the flower itself.
     * @param flower The flower to ckeck.
     * @return Collection of flower edge neighbors.
     */
    public Collection<ColoredFlower> getParentFlowerSetFlowerEdgeNeighbors(ColoredFlower flower) {
        if (flower.getParentFlowerSet() != null) {
            return flower.getParentFlowerSet().getFlowerEdgeNeighbors();
        } else {
            return flower.getFlowerEdgeNeighbors();
        }
    }

    /**
     * Returns the flower node neighbors of the parent FlowerSet, if existing.
     * Else, returns the flower node neighbors of the flower itself.
     * @param flower The flower to ckeck.
     * @return Collection of flower node neighbors.
     */
    public Collection<ColoredFlower> getParentFlowerSetFlowerNodeNeighbors(ColoredFlower flower) {
        if (flower.getParentFlowerSet() != null) {
            return flower.getParentFlowerSet().getFlowerNodeNeighbors();
        } else {
            return flower.getFlowerNodeNeighbors();
        }
    }

    /**
     * A flower move consists in planting two flowers. Given a fixed first flower,
     * this method generates all possible flower tupels for this flower.
     * @return A collection of flower moves (i.e. flower tupels).
     */
    @Override
    public Collection<Move> generateMoves() {
        if (secondFlower != null) {
            throw new IllegalAccessError();
        }

        Collection<Move> possibleMoves = new ArrayList<Move>();
        Flower first = firstFlower;

        // Get all the flowers which are still possible after setting the first flower of the move.
        // I.e. all flowers, that are in <col.>possibleFlowers list on the board but not stored as impossible in the increment.
        for (ColoredFlower cf : possibleFlowers.values()) {
            if ((color == FGWType.RED && !firstIncrement.excludeRedPossibleFlowersContains(cf))
                    || (color == FGWType.BLUE && !firstIncrement.excludeBluePossibleFlowersContains(cf))) {

                // ... and create the possible move
                Move newMove = new Move(first, cf);
                if (first == newMove.getFirstFlower()) {
                    possibleMoves.add(newMove);
                }
            }
        }
        return possibleMoves;
    }

    /**
     * This is a method to make a flower move. The changings stored in the increment
     * of the first flower are made on the board, then the neighborhood of the second
     * flower is checked and these changings are also made on the board.
     * So, after the move, all possible lists are up to date.
     * @param move The flower move to be made.
     */
    @Override
    public void make (final Move move) {
        if (!firstFlower.equals(move.getFirstFlower())) {
            throw new IllegalMoveException("First flower not the same!");
        }
        Flower dummy = move.getSecondFlower();
        secondFlower = board.getColoredFlower(dummy);

        if (secondFlower == null) {
            throw new IllegalMoveException("Second flower is null!");
        } else if (secondIncrements.containsKey(dummy)) {
            firstIncrement.make();
            secondIncrements.get(dummy).make();
        } else {
            firstFlower.setFlowerType(color);
            checkDitchNeighborhood(firstFlower, firstIncrement);
            firstFlower.setFlowerType(FGWType.UNCOLORED);
            firstIncrement.make();
            flowerSetsToDelete = new HashSet<FlowerSet>();
            flowerSetToUpdate = null;

            FGWIncrement secondIncrement = new FGWIncrement(board);
            secondFlower.setFlowerType(color);
            assignFlowerToFlowerSet(secondFlower, secondIncrement);
            checkFlowerNeighborhood(secondFlower, secondIncrement);
            checkDitchNeighborhood(secondFlower, secondIncrement);
            secondFlower.setFlowerType(FGWType.UNCOLORED);
            secondIncrement.make();
            secondIncrements.put(dummy, secondIncrement);
        }


        // make the move and the corresponding changes of possible flowers
        // and ditches the via the increments.


    }

    /**
     * Undoes a move using the increment.
     */
    @Override
    public void unmake(Move move) {
        if (!firstFlower.equals(move.getFirstFlower()) || !secondIncrements.containsKey(move.getSecondFlower())) {
            throw new IllegalArgumentException("We would unmake a wrong flower move!");
        }
        secondIncrements.get(move.getSecondFlower()).unmake();
        firstIncrement.unmake();
    }

    /**
     * This method assigns the newly planted flower to a FlowerSet.
     * Depending on the number of colored edge neighbors of the new flower, the
     * corresponding FlowerSets have to be updated (generated / merged / saved as
     * "to delete" in the increment).
     * @param newFlower Newly planted flower.
     * @param increment The increment where the changes are stored.
     */
    private void assignFlowerToFlowerSet (ColoredFlower newFlower, FGWIncrement increment) {
        Collection<ColoredFlower> flowerEdgeNeighbors = newFlower.getFlowerEdgeNeighbors();
        ArrayList<ColoredFlower> coloredEdgeNeighbors =  new ArrayList<ColoredFlower>();
        // Count number of equally colored edge neighbors of the flower:
        for (ColoredFlower c : flowerEdgeNeighbors) {
            if (c.getFlowerType() == color) {
                coloredEdgeNeighbors.add(c);
            }
        }

        // Case 1: no edge neighbors in the same color.
        // Create new FlowerSet containing only the new flower.
        if (coloredEdgeNeighbors.size() == 0) {
            flowerSetToUpdate = new FlowerSet(newFlower);
        } else {
        // Case 2: at least one edge neighbor is in the same color.
        // We're working in a copy of the FlowerSet of the neighbor. Therefore
        // we don't make any changes on the board. In particular, we do add
        // the new flower to the existing FlowerSet but we do not
        // update the reference to the parentFlowerSet of the new flower.
            flowerSetToUpdate = new FlowerSet(coloredEdgeNeighbors.get(0).getParentFlowerSet());
            flowerSetToUpdate.plant(newFlower);
            flowerSetsToDelete.add(coloredEdgeNeighbors.get(0).getParentFlowerSet());
            if (coloredEdgeNeighbors.size() > 1) {
                for (int i = 1; i < coloredEdgeNeighbors.size(); i++) {
                    flowerSetToUpdate.plant(coloredEdgeNeighbors.get(i).getParentFlowerSet());
                    flowerSetsToDelete.add(coloredEdgeNeighbors.get(i).getParentFlowerSet());
                }
            }
        }

        // Save the changes in the increment
        increment.setTheNewColoredFlower(newFlower);
        increment.setTypeOfNewColoredFlower(color);
        increment.addTheFlowerSet(flowerSetToUpdate);
        increment.excludeTheFlowerSet(flowerSetsToDelete);
    }

    /**
     * After planting a flower, the new flower has to notify its flower neighbors.
     * The lists of possible flowers have to be updated (via the increment).
     * @param newFlower The newly planted flower.
     * @param increment The increment where the changes are stored."
     */
    private void checkFlowerNeighborhood(ColoredFlower newFlower, FGWIncrement increment) {
        if (flowerSetToUpdate.getSize() == 4) {
        // Garden
        // Until now, only edgeNeighbors were removed from the possible flowers.
        // If we have a garden, also the nodeNeighbors must be set to impossible.
        excludePossibleFlower(flowerSetToUpdate.getFlowerNodeNeighbors(), increment);
        excludePossibleFlower(flowerSetToUpdate.getFlowerEdgeNeighbors(), increment);
        } else {
            // Check, if the FlowerSet has node neighbors, which have edge neighbors belonging to
            // a FlowerSet of size 3.
            for (ColoredFlower nodeNeighbor : flowerSetToUpdate.getFlowerNodeNeighbors()) {
                for (ColoredFlower cf : nodeNeighbor.getFlowerEdgeNeighbors()) {
                    if (cf.getFlowerType() == color && getParentFlowerSetSize(cf) == 3 && !flowerSetToUpdate.contains(cf)) {
                        excludePossibleFlower(nodeNeighbor, increment);
                    }
                }
            }
            // When a FlowerSet has size 3 AND has at least one nodeNeighbor, then it
            // cannot be extended to a garden. Thus, store all edgeNeighbors of the
            // FlowerSet as impossible in the increment.
            if (flowerSetToUpdate.getSize() == 3) {
                if (countColoredNodeNeighbors (flowerSetToUpdate) >= 1) {
                    // If one node neighbor in the same color found, remove all edgeNeighbors.
                    excludePossibleFlower(flowerSetToUpdate.getFlowerEdgeNeighbors(), increment);
                } else {
                    // If the FlowerSet has no colored nodeNeighbor, check if there is an edgeNeighbor in which
                    // it cannot be extended to a garden (because this edge neighbor has another colored nodeNeighbor).
                    for (ColoredFlower edgeNeighbor : flowerSetToUpdate.getFlowerEdgeNeighbors()) {
                        for (ColoredFlower cf : edgeNeighbor.getFlowerNodeNeighbors()) {
                            if (cf.getFlowerType() == color && !flowerSetToUpdate.contains(cf)) {
                                excludePossibleFlower(edgeNeighbor, increment);
                            }
                        }
                    }
                }
            } else {
                //For checking whether edge neighbors of a FlowerSet of size 2 or 1 are still
                // possible, we have to check it's node neighboring flowers.
                updateFlowerSetNodeNeighbors(flowerSetToUpdate, increment);
            }

            // We also have to check "the other way around": if the new flower becomes a node neighbor
            // of an existing FlowerSet, the flower neighbors of this neighboring FlowerSet have to be updated aswell.
            for (ColoredFlower nodeNeighbor : flowerSetToUpdate.getFlowerNodeNeighbors()) {
                if (nodeNeighbor.getFlowerType() == color && getParentFlowerSetSize(nodeNeighbor) == 3 && !flowerSetToUpdate.contains(nodeNeighbor)) {
                    excludePossibleFlower(getParentFlowerSetFlowerEdgeNeighbors(nodeNeighbor), increment);
                }

                if (newFlower.getFlowerNodeNeighbors().contains(nodeNeighbor) && nodeNeighbor.getFlowerType() == color) {
                    for (ColoredFlower cf : getParentFlowerSetFlowerEdgeNeighbors(nodeNeighbor)) {
                        int coloredNeighbors = 0;
                        for (ColoredFlower cfN : cf.getFlowerEdgeNeighbors()) {
                            if (cfN.getFlowerType() == color && !cfN.equals(newFlower)) {
                                coloredNeighbors += getParentFlowerSetSize(cfN);
                            }
                        }
                        if (coloredNeighbors >= 3) {
                            excludePossibleFlower(cf, increment);
                        }
                    }
                }
            }

            // Update the edgeNeighbors, but only if it's still possible.
            for (ColoredFlower edgeNeighbor : flowerSetToUpdate.getFlowerEdgeNeighbors()) {
                // Check, if still possible on board.
                if (possibleFlowers.containsValue(edgeNeighbor)) {
                    // Check, if already set to impossible in the increment.
                    if ((color == FGWType.RED && !increment.excludeRedPossibleFlowersContains(edgeNeighbor))
                            || (color == FGWType.BLUE && !increment.excludeBluePossibleFlowersContains(edgeNeighbor))) {
                        updateTheEdgeNeighbor(edgeNeighbor, increment);
                    }
                }
            }
        }
    }

    /**
     * This method updates a particular edgeNeighbor (of the flowerSetToUpdate).
     * The method counts the size of the neighboring FlowerSets corresponding to
     * the edgeNeighbors of this particular edge neighbor.
     * If this particular edge neighboring flower is not possible anymore due to
     * the game rules, it is set to impossible.
     * @param edgeNeighbor EdgeNeighbor to be checked.
     * @param increment The increment where the changes are stored.
     */
    private void updateTheEdgeNeighbor(ColoredFlower edgeNeighbor, FGWIncrement increment) {
        // count the size of each Neighboring FlowerSet
        int countSizeOfNeighboringFlowerSets = 0;
        // store the flowers of our updated FlowerSet
        Collection<ColoredFlower> flowersOfOurUpdateSet = flowerSetToUpdate.getFlowers();
        // iterate over the EdgeNeighbors of the argument
        for (ColoredFlower cf : edgeNeighbor.getFlowerEdgeNeighbors()) {
            // if the neighbor cf is one of the flowers in our updated FlowerSet, consider
            if (flowersOfOurUpdateSet.contains(cf)) {
                countSizeOfNeighboringFlowerSets += flowersOfOurUpdateSet.size();
            // if the neighbor cf not one of the flowers in our updated FlowerSet, consider
            } else if (cf.getFlowerType() == color) {
                countSizeOfNeighboringFlowerSets += getParentFlowerSetSize(cf);
            }
        }

        // If the sum over the sizes of the neighboring FlowerSets equals 3,
        // we have to check whether there are colored node neighbors of the
        // colored edge neighbor of the particular edge neighbor.
        // If so, this particular edgeNeighbor has to be set impossible.
        if (countSizeOfNeighboringFlowerSets == 3) {
            for (ColoredFlower enOfEdgeNeighbor : edgeNeighbor.getFlowerEdgeNeighbors()) {
                if (enOfEdgeNeighbor.getFlowerType() == color) {
                    for (ColoredFlower cf : getParentFlowerSetFlowerNodeNeighbors(enOfEdgeNeighbor)) {
                        if (flowerSetToUpdate.contains(cf)) {
                            if (cf.getFlowerType() == color &&
                                    !flowerSetToUpdate.getFlowerEdgeNeighbors().contains(edgeNeighbor) &&
                                    !flowerSetToUpdate.getFlowerEdgeNeighbors().contains(enOfEdgeNeighbor)) {
                                excludePossibleFlower(edgeNeighbor, increment);
                                break;
                            }
                        } else if (cf.getFlowerType() == color &&
                                    !getParentFlowerSetFlowerEdgeNeighbors(cf).contains(edgeNeighbor) &&
                                    !getParentFlowerSetFlowerEdgeNeighbors(cf).contains(enOfEdgeNeighbor)) {
                            excludePossibleFlower(edgeNeighbor, increment);
                            break;
                        }
                    }
                }
            }
        }
        // If the sum over the sizes of the neighboring FlowerSets is bigger equal 4, then
        // the current FlowerSet cannot have any more edgeNeighbors.
        // Hence, set them all to impossible.
        if (countSizeOfNeighboringFlowerSets >= 4) {
            excludePossibleFlower(flowerSetToUpdate.getFlowerEdgeNeighbors(), increment);
        }
    }

    /**
     * For a FlowerSet of size 2, this method checks whether it has two colored
     * flower node neighbors. If so, the flowers "in between" have to be removed from
     * the possible flowers list in the increment (because otherwise  one could get
     * a garden with a node neighbored flower).
     * @param flowerset The FlowerSet to be checked.
     * @param increment The increment where the changes are stored.
     */
    public void updateFlowerSetNodeNeighbors(FlowerSet flowerset, FGWIncrement increment) {
        if (flowerSetToUpdate.getSize() == 2) {
            if (countColoredNodeNeighbors(flowerSetToUpdate) >= 2) {
                for (ColoredFlower cf : flowerSetToUpdate.getFlowerEdgeNeighbors()) {
                    for (ColoredFlower cfN : cf.getFlowerEdgeNeighbors()) {
                        if (!flowerSetToUpdate.contains(cfN) && cfN.getFlowerType() == color) {
                            // only remove if cfN is NOT contained in the flowerSetToUpdate
                            excludePossibleFlower(cf, increment);
                        }
                    }
                }
            }
        }
    }

    /**
     * A method to set a flower as inpossible for the corresponding color in the increment.
     * @param flower The flower to exclude.
     * @param increment The increment where the changes are stored.
     */
    public void excludePossibleFlower(ColoredFlower flower, FGWIncrement increment) {
        if (color == FGWType.RED) {
            increment.excludeTheRedPossibleFlower(flower);
        } else {
            increment.excludeTheBluePossibleFlower(flower);
        }
    }

    /**
     * A method to calculate the number of equally colored flower node neighbors
     * of a FlowerSet.
     * @param flowerset The flower to be checked.
     * @return The number of colored flower node neighbors.
     */
    public int countColoredNodeNeighbors(FlowerSet flowerset) {
        int numberColoredNodeNeighbors = 0;
        for (ColoredFlower cf : flowerset.getFlowerNodeNeighbors()) {
            if (cf.getFlowerType() == color) {
                numberColoredNodeNeighbors++;
            }
        }
        return numberColoredNodeNeighbors;
    }

    /**
     * A method to set several flowers as inpossible for the corresponding color in the increment.
     * @param flowers The collection of flowers to exclude.
     * @param increment The increment where the changes are stored.
     */
    public void excludePossibleFlower(Collection<ColoredFlower> flowers, FGWIncrement increment) {
        if (color == FGWType.RED) {
            increment.excludeTheRedPossibleFlower(flowers);
        } else {
            increment.excludeTheBluePossibleFlower(flowers);
        }
    }

    /**
     * After planting a flower, this method checks the ditch neighborhood of the updated FlowerSet.
     * All ditch edge neighbors have to be set to fallow and all ditch node neighbors have to be checked
     * again, as after planting a flower, the possible ditches for a color change (number of them can decrease or increase).
     * @param newFlower Newly planted flower.
     * @param increment The increment where the changes are stored.
     */
    public void checkDitchNeighborhood(ColoredFlower newFlower, FGWIncrement increment) {
        // after setting a flower, the ditch edge neighors of the new FlowerSet have to be removed:
        increment.setDitchToFallow(newFlower.getDitchEdgeNeighbors());

        // for the (uncolored) node edge neighbors check, if there connect this flowerset to another flowerset.
        for (ColoredDitch cd : newFlower.getDitchNodeNeighbors()) {
            if (cd.getDitchType() == FGWType.UNCOLORED && cd.hasColoredFlowerOnBothSides(color)) {
                increment.addToPossibleDitches(cd, color);
            }
        }
    }
}
