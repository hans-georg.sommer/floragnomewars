package flowerwarspp.boardmechanics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;

import flowerwarspp.preset.Position;
import flowerwarspp.preset.Flower;

/**
* Adds extra functionality: <br>
* - status variable (if flower is red, blue, uncolored or fallow), <br>
* - edge neighboring flowers, <br>
* - node neighboring flowers, <br>
* - edge neighboring ditches, <br>
* - node neighboring ditches, <br>
* - parentFlowerSet.
* @author Charlotte Ackva, Felix Spuehler, Martin Heide
**/

public class ColoredFlower extends Flower {
    /** Contains the edge neighboring flowers of the flower. */
    private Collection<ColoredFlower> flowerEdgeNeighbors;
    /** Contains the node neighboring flowers of the flower. */
    private Collection<ColoredFlower> flowerNodeNeighbors;
    /** Contains the edge neighboring ditches of the flower. */
    private Collection<ColoredDitch> ditchEdgeNeighbors;
    /** Contains the node neighboring ditches of the flower. */
    private Collection<ColoredDitch> ditchNodeNeighbors;
    /** Contains the information about the status of the flower (red, blue, uncolored or fallow). */
    private FGWType flowerType;
    /** Contains the information about the parent flower set where the flower is contained in. */
    private FlowerSet parentFlowerSet;

    /** Constructs a ColoredFlower between the three specified positions. <br>
     * The flowerType is set to uncolored.
     * @param first One position.
     * @param second Another position.
     * @param third Third position.
     */
    public ColoredFlower (final Position first, final Position second, final Position third) {
        super(first,second,third);
        flowerType = FGWType.UNCOLORED;
    }

    /** Returns the flower node neighbors of the flower.
     * @return Unmodifiable collection of ColoredFlowers.
     **/
    public Collection<ColoredFlower> getFlowerNodeNeighbors() {
        return Collections.unmodifiableCollection(flowerNodeNeighbors);
    }

    /** Returns the flower edge neighbors of the flower.
     * @return Unmodifiable collection of ColoredFlowers.
     **/
    public Collection<ColoredFlower> getFlowerEdgeNeighbors() {
        return Collections.unmodifiableCollection(flowerEdgeNeighbors);
    }

    /** Returns the ditch edge neighbors of the flower.
     * @return Unmodifiable collection of ColoredDitches.
     **/
    public Collection<ColoredDitch> getDitchEdgeNeighbors() {
        return Collections.unmodifiableCollection(ditchEdgeNeighbors);
    }

    /** Returns the ditch node neighbors of the flower.
     * @return Unmodifiable collection of ColoredDitches.
     **/
    public Collection<ColoredDitch> getDitchNodeNeighbors() {
        return Collections.unmodifiableCollection(ditchNodeNeighbors);
    }

    /**
    * Returns the set of all neighboring flowers.
    * @return Unmodifiable collection of ColoredFlowers.
    */
    public Collection<ColoredFlower> getAllFlowerNeighbors() {
        Collection<ColoredFlower> setOfNeighbors = new ArrayList<ColoredFlower>(getFlowerNodeNeighbors());
        setOfNeighbors.addAll(getFlowerEdgeNeighbors());
        return Collections.unmodifiableCollection(setOfNeighbors);
    }

    /**
     * Checks if the argument flower is an edge neighbor of the given flower.
     * @param flower The flower one wants to check.
     * @return True if the argument is contained in the flower edge neighbors of the object.
     **/
    public boolean isEdgeNeighbor(ColoredFlower flower) {
        return flowerEdgeNeighbors.contains(flower);
    }

    /** Sets the flower node neighbors of the flower.
     * @param flowers Collection of ColoredFlowers.
     **/
    public void setFlowerNodeNeighbors(Collection<ColoredFlower> flowers) {
        flowerNodeNeighbors = flowers;
    }

    /** Sets the flower edge neighbors of the flower.
     * @param flowers Collection of ColoredFlowers.
     **/
    public void setFlowerEdgeNeighbors(Collection<ColoredFlower> flowers) {
        flowerEdgeNeighbors = flowers;
    }

    /** Sets the ditch edge neighbors of the flower.
     * @param ditches Collection of ColoredDitches.
     **/
    public void setDitchEdgeNeighbors(Collection<ColoredDitch> ditches) {
        ditchEdgeNeighbors = ditches;
    }

    /** Sets the ditch node neighbors of the flower.
     * @param ditches Collection of ColoredDitches.
     **/
    public void setDitchNodeNeighbors(Collection<ColoredDitch> ditches) {
        ditchNodeNeighbors = ditches;
    }

    /** Sets the type of the flower (red, blue, uncolored or fallow).
     * @param type FGWType - the new type of the flower.
     **/
    public void setFlowerType(FGWType type) {
        flowerType = type;
    }

    /** Returns the type of the flower (red, blue, uncolored or fallow).
     *  @return FGWType of the flower.
     */
    public FGWType getFlowerType() {
        return flowerType;
    }

    /** Assigns the flower to a FlowerSet.
     * @param parent The FlowerSet to assign to.
     **/
    public void setParentFlowerSet(FlowerSet parent) {
        parentFlowerSet = parent;
    }

    /** Returns the parent FlowerSet of the flower.
     * @return Parent FlowerSet where the flower is contained in.
     **/
    public FlowerSet getParentFlowerSet() {
        return parentFlowerSet;
    }

    /**
     * Calculates the hash code of a flower.
     * @param first One position.
     * @param second Another position.
     * @param third Third position.
     * @return Hash code.
     */
    public static int hashCode(Position first, Position second, Position third) {
        // Keine Pruefung, ob Blume ueberhaupt im Feld...
        Flower dummyFlower = new Flower(first, second, third);
        return dummyFlower.hashCode();

    }
}
