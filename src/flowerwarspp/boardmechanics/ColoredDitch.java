package flowerwarspp.boardmechanics;

import java.util.Collections;
import java.util.Collection;

import flowerwarspp.preset.Position;
import flowerwarspp.preset.Ditch;


/**
* Adds extra functionality: <br>
* - status variable (if ditch is red, blue, uncolored or fallow), <br>
* - has neighboring ditches, <br>
* - has edge neighboring flowers, <br>
* - has node neighboring flowers. <br>
* @author Charlotte Ackva, Felix Spuehler, Martin Heide
**/
public class ColoredDitch extends Ditch {
    /** Holds the edge neighboring flowers of the ditch. */
    private Collection<ColoredFlower> flowerEdgeNeighbors;
    /** Holds the node neighboring flowers of one side (i.e. of one position) of the ditch.*/
    private Collection<ColoredFlower> flowerNodeNeighborsOne;
    /** Holds the node neighboring flowers of the other side of the ditch. */
    private Collection<ColoredFlower> flowerNodeNeighborsTwo;
    /** Holds the neighboring ditches of the ditch. */
    private Collection<ColoredDitch> ditchNeighbors;
    /** Holds the information about the status of the ditch (red, blue, uncolored or fallow).*/
    private FGWType ditchType;

    /**
     * Constructs a ColoredDitch between the two specified positions. <br>
     * The ditchType is set to uncolored.
     * @param first One Position.
     * @param second Second Positon.
     */
    public ColoredDitch(final Position first, final Position second) {
        super(first, second);
        ditchType = FGWType.UNCOLORED;
    }

    /**
     * Returns the flower edge neighbors of the Ditch.
     * @return Unmodifiable collection of ColoredFlowers.
    **/
    public Collection<ColoredFlower> getFlowerEdgeNeighbors() {
        return Collections.unmodifiableCollection(flowerEdgeNeighbors);
    }

    /**
     * Sets the flower edge neighbors of the ditch.
     * @param flowers Collection of ColoredFlowers.
     **/
    public void setFlowerEdgeNeighbors(Collection<ColoredFlower> flowers) {
        flowerEdgeNeighbors = flowers;
    }

    /**
     * Returns the flower node neighbors at one side of the ditch.
     * @return Unmodifiable collection of ColoredFlowers.
    **/
    public Collection<ColoredFlower> getFlowerNodeNeighborsOne() {
        return Collections.unmodifiableCollection(flowerNodeNeighborsOne);
    }

    /**
     * Sets the flower node neighbors at one side of the ditch.
     * @param flowers Collection of ColoredFlowers.
     **/
    public void setFlowerNodeNeighborsOne(Collection<ColoredFlower> flowers) {
        flowerNodeNeighborsOne = flowers;
    }

    /**
     * Returns the flower node neighbors at the other side of the ditch.
     * @return Unmodifiable collection of ColoredFlowers.
    **/
    public Collection<ColoredFlower> getFlowerNodeNeighborsTwo() {
        return Collections.unmodifiableCollection(flowerNodeNeighborsTwo);
    }

    /**
     * Sets the flower node neighbors at the other side of the ditch.
     * @param flowers Collection of ColoredFlowers.
     **/
    public void setFlowerNodeNeighborsTwo(Collection<ColoredFlower> flowers) {
        flowerNodeNeighborsTwo = flowers;
    }

    /**
     * Returns the ditch (edge) neighbors of the ditch.
     * @return Unmodifiable collection of ColoredDitches.
     **/
    public Collection<ColoredDitch> getDitchNeighbors() {
        return Collections.unmodifiableCollection(ditchNeighbors);
    }

    /**
     * Sets the ditch (edge) neighbors of the ditch.
     * @param ditches Collection of ColoredDitches.
     **/
    public void setDitchNeighbors(Collection<ColoredDitch> ditches) {
        ditchNeighbors = ditches;
    }

    /**
     * Returns the type of the ditch (red, blue, uncolored or fallow).
     *  @return FGWType of the ditch.
     */
    public FGWType getDitchType() {
        return ditchType;
    }

    /**
     * Sets the type of the ditch (red, blue, uncolored or fallow).
     * @param type FGWType - the new type of the ditch.
     **/
    public void setDitchType(FGWType type) {
        ditchType = type;
    }

    /**
     * Checks whether a ditch connects two flowers of the same color.
     * @param color FGWType - color of the flowers to be looked for.
     * @return True if ditch connects two flowers of the specified color.
     */
    public boolean hasColoredFlowerOnBothSides(FGWType color) {
        for (ColoredFlower cf : flowerNodeNeighborsOne) {
            if (cf.getFlowerType() == color) {
                for (ColoredFlower flower : flowerNodeNeighborsTwo) {
                    if (flower.getFlowerType() == color) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Calculates the hash code of a ditch.
     * @param first One position.
     * @param second Second position.
     * @return Hashcode.
     */
    public static int hashCode(Position first, Position second) {
        // Keine Pruefung, ob Graben ueberhaupt im Feld...
        Ditch dummyDitch = new Ditch(first, second);
        return dummyDitch.hashCode();
    }
}
