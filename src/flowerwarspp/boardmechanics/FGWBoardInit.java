package flowerwarspp.boardmechanics;

import flowerwarspp.preset.Position;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Initializes the board with flowers and ditches as ColoredFlowers and ColoredDitches.
 * @version 1.0
 * @author Felix Spuehler
 */
public class FGWBoardInit {
    /** Size of the board. */
    private final int boardSize;
    /** Contains all flowers with hashcode as key. */
    private HashMap<Integer, ColoredFlower> allFlowers;
    /** Contains all ditches with hashcode as key. */
    private HashMap<Integer, ColoredDitch> allDitches;

    /**
     * Constructor which first initizializes all flowers and ditches.
     * Afterwards it sets references to all neighbors.
     * @param boardSize Size of the board.
     */
    public FGWBoardInit(int boardSize) {
        this.boardSize = boardSize;
        allFlowers = new HashMap<Integer, ColoredFlower>();
        allDitches = new HashMap<Integer, ColoredDitch>();

        initializeFlowersAndDitches();
        setNeighborsOfFlowers();
        setNeighborsOfDitches();
    }

    /**
    * Returns a copy of uncolored flowers.
    * @return The uncolored flowers.
    */
    public HashMap<Integer, ColoredFlower> getAllFlowers() {
        return new HashMap<Integer, ColoredFlower>(allFlowers);
    }

    /**
     * Returns a copy of uncolored ditches.
     * @return The uncolored ditches.
     */
    public HashMap<Integer, ColoredDitch> getAllDitches() {
        return new HashMap<Integer, ColoredDitch>(allDitches);
    }

    /**
     * Initializes all flowers and ditches as ColoredFlowers and ColoredDitches.
     */
    public void initializeFlowersAndDitches() {
        Position first;
        Position second;
        Position third;
        ColoredFlower dummyColoredFlower;
        ColoredDitch dummyColoredDitch;

        for (int col = 1; col <= boardSize; col++) {
            for (int row = 1; col + row <= boardSize + 1; row++) {
                /* over the edge (col, row) - (col + 1, row) */
                first = new Position(col, row);
                second = new Position(col + 1, row);
                third = new Position(col, row + 1);
                // flower
                dummyColoredFlower = new ColoredFlower(first, second, third);
                allFlowers.put(dummyColoredFlower.hashCode(), dummyColoredFlower);
                // ditches
                dummyColoredDitch = new ColoredDitch(first, second);
                allDitches.put(dummyColoredDitch.hashCode(), dummyColoredDitch);
                dummyColoredDitch = new ColoredDitch(first, third);
                allDitches.put(dummyColoredDitch.hashCode(), dummyColoredDitch);
                dummyColoredDitch = new ColoredDitch(second, third);
                allDitches.put(dummyColoredDitch.hashCode(), dummyColoredDitch);

                /* under this edge */
                if (row > 1) {
                    third = new Position(col + 1, row - 1);
                    dummyColoredFlower = new ColoredFlower(first, second, third);
                    allFlowers.put(dummyColoredFlower.hashCode(), dummyColoredFlower);
                }
            }
        }
    }

    /**
     * Sets the edge and node neighboring flowers and ditches (references) for each flower.
     */
    public void setNeighborsOfFlowers() {
        Position first;
        Position second;
        Position third;
        Position partner;
        ColoredFlower currentFlower;
        ArrayList<ColoredDitch> dummyDitches;
        ArrayList<ColoredFlower> dummyFlowers;

        for (int col = 1; col <= boardSize; col++) {
            for (int row = 1; col + row <= boardSize + 1; row++) {
                /* flower over an edge */
                first = new Position(col, row);
                second = new Position(col + 1, row);
                third = new Position(col, row + 1);
                currentFlower = allFlowers.get(ColoredFlower.hashCode(first, second, third));

                // Edge neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, second)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, third)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, third)));
                currentFlower.setDitchEdgeNeighbors(dummyDitches);

                // Node neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                if (col > 1) {
                    partner = new Position(col - 1, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col - 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col - 1, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                if (row > 1) {
                    partner = new Position(col, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col + 1, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 2, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                }
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 2, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                currentFlower.setDitchNodeNeighbors(dummyDitches);

                // Edge neighboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                if (row > 1) {
                    Position newThird = new Position(col + 1, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, newThird)));
                }
                if (col > 1) {
                    Position newSecond = new Position(col - 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, newSecond, third)));
                }
                if (row + col < boardSize + 1) {
                    Position newFirst = new Position(col + 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(newFirst, second, third)));
                }
                currentFlower.setFlowerEdgeNeighbors(dummyFlowers);

                // Node neighboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                if (col > 1) {
                    first = new Position(col - 1, row + 1);
                    second = new Position(col - 1, row);
                    third = new Position(col, row);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col, row + 1);
                    third = new Position(col - 1, row + 2);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (row > 1) {
                    first = new Position(col + 1, row - 1);
                    second = new Position(col, row - 1);
                    third = new Position(col, row);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col + 2, row - 1);
                    third = new Position(col + 1, row);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (col + row < boardSize + 1) {
                    first = new Position(col + 1, row + 1);
                    second = new Position(col + 1, row);
                    third = new Position(col + 2, row);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col, row + 1);
                    third = new Position(col, row + 2);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (col > 1 && row > 1) {
                    first = new Position(col - 1, row);
                    second = new Position(col, row);
                    third = new Position(col, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (col > 1 && col + row < boardSize + 1) {
                    first = new Position(col - 1, row + 2);
                    second = new Position(col, row + 2);
                    third = new Position(col, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (row > 1 && col + row < boardSize + 1) {
                    first = new Position(col + 1, row);
                    second = new Position(col + 2, row);
                    third = new Position(col + 2, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                currentFlower.setFlowerNodeNeighbors(dummyFlowers);

                /* flower under an edge */
                if (row == 1) {
                    continue;
                }
                first = new Position(col, row);
                second = new Position(col + 1, row);
                third = new Position(col + 1, row - 1);
                currentFlower = allFlowers.get(ColoredFlower.hashCode(first, second, third));

                // Edge neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, second)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, third)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, third)));
                currentFlower.setDitchEdgeNeighbors(dummyDitches);

                // Node neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                partner = new Position(col, row - 1);
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                partner = new Position(col, row + 1);
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                partner = new Position(col + 2, row - 1);
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));

                if (col > 1) {
                    partner = new Position(col - 1, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col - 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                }
                if (row > 2) {
                    partner = new Position(col + 1, row - 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col + 2, row - 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 2, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                }
                currentFlower.setDitchNodeNeighbors(dummyDitches);

                // Edge neighboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                Position newThird = new Position(col, row + 1);
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, newThird)));
                Position newSecond = new Position(col, row - 1);
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, newSecond, third)));
                Position newFirst = new Position(col + 2, row - 1);
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(newFirst, second, third)));
                currentFlower.setFlowerEdgeNeighbors(dummyFlowers);

                // Node neighboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                if (row > 2) {
                    first = new Position(col + 1, row - 1);
                    second = new Position(col, row - 1);
                    third = new Position(col + 1, row - 2);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col + 1, row - 2);
                    third = new Position(col + 2, row - 2);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col + 2, row - 1);
                    third = new Position(col + 2, row - 2);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (col > 1) {
                    first = new Position(col, row);
                    second = new Position(col - 1, row);
                    third = new Position(col, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col - 1, row);
                    third = new Position(col - 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col, row + 1);
                    third = new Position(col - 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                if (col + row < boardSize + 1) {
                    first = new Position(col + 1, row);
                    second = new Position(col + 1, row + 1);
                    third = new Position(col, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col + 2, row);
                    third = new Position(col + 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                    second = new Position(col + 2, row);
                    third = new Position(col + 2, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                }
                currentFlower.setFlowerNodeNeighbors(dummyFlowers);
            }
        }
    }

    /**
     * Sets the edge and node neighboring flowers and neighboring ditches for each ditch.
     */
    public void setNeighborsOfDitches() {
        Position first;
        Position second;
        Position third;
        Position partner;
        Position partnerOne;
        Position partnerTwo;

        ColoredDitch currentDitch;
        ArrayList<ColoredDitch> dummyDitches;
        ArrayList<ColoredFlower> dummyFlowers;
        ArrayList<ColoredFlower> dummyFlowersOne;
        ArrayList<ColoredFlower> dummyFlowersTwo;

        for (int col = 1; col <= boardSize; col++) {
            for (int row = 1; col + row <= boardSize + 1; row++) {
                first = new Position(col, row);
                second = new Position(col + 1, row);
                third = new Position(col, row + 1);

                /* first ditch */
                currentDitch = allDitches.get(ColoredDitch.hashCode(first, second));
                // neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, third)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, third)));
                if (row > 1) {
                    partner = new Position(col + 1, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col + 2, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                }
                if (col > 1) {
                    partner = new Position(col - 1, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col - 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                }
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 2, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                }
                currentDitch.setDitchNeighbors(dummyDitches);

                // edge neigboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                if (row > 1) {
                    partner = new Position(col + 1, row - 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, partner)));
                }
                currentDitch.setFlowerEdgeNeighbors(dummyFlowers);

                // node neighboring flowers
                dummyFlowersOne = new ArrayList<ColoredFlower>();
                dummyFlowersTwo = new ArrayList<ColoredFlower>();
                if (col > 1) {
                    partnerOne = new Position(col - 1, row + 1);
                    partnerTwo = new Position(col, row + 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col - 1, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                }
                if (col > 1 && row > 1) {
                    partnerOne = new Position(col - 1, row);
                    partnerTwo = new Position(col, row - 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                }
                if (row > 1) {
                    partnerOne = new Position(col + 1, row - 1);
                    partnerTwo = new Position(col, row - 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col + 2, row - 1);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                }
                if (row > 1 && col + row < boardSize + 1) {
                    partnerOne = new Position(col + 2, row - 1);
                    partnerTwo = new Position(col + 2, row);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                }
                if (col + row < boardSize + 1) {
                    partnerOne = new Position(col + 1, row + 1);
                    partnerTwo = new Position(col + 2, row);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col + 1, row + 1);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                }
                currentDitch.setFlowerNodeNeighborsOne(dummyFlowersOne);
                currentDitch.setFlowerNodeNeighborsTwo(dummyFlowersTwo);

                /* second ditch */
                currentDitch = allDitches.get(ColoredDitch.hashCode(second, third));
                // neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, second)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, third)));
                if (row > 1) {
                    partner = new Position(col + 1, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 2, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                }
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 2, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    partner = new Position(col + 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                if (col > 1) {
                    partner = new Position(col - 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col - 1, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                currentDitch.setDitchNeighbors(dummyDitches);

                // edge neighboring flowers
                dummyFlowers = new ArrayList<ColoredFlower>();
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(second, third, partner)));
                }
                currentDitch.setFlowerEdgeNeighbors(dummyFlowers);

                // node neighboring flowers
                dummyFlowersOne = new ArrayList<ColoredFlower>();
                dummyFlowersTwo = new ArrayList<ColoredFlower>();
                if (row > 1) {
                    partnerOne = new Position(col + 1, row - 1);
                    partnerTwo = new Position(col, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col + 2, row - 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                }
                if (row > 1 && col + row < boardSize + 1) {
                    partnerOne = new Position(col + 2, row - 1);
                    partnerTwo = new Position(col + 2, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                }
                if (col + row < boardSize + 1) {
                    partnerOne = new Position(col + 1, row + 1);
                    partnerTwo = new Position(col + 2, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(second, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                }
                if (col > 1 && col + row < boardSize + 1) {
                    partnerOne = new Position(col, row + 2);
                    partnerTwo = new Position(col - 1, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                }
                if (col > 1) {
                    partnerOne = new Position(col - 1, row + 1);
                    partnerTwo = new Position(col - 1, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col, row);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                }
                currentDitch.setFlowerNodeNeighborsOne(dummyFlowersOne);
                currentDitch.setFlowerNodeNeighborsTwo(dummyFlowersTwo);

                /* third ditch */
                currentDitch = allDitches.get(ColoredDitch.hashCode(first, third));
                // neighboring ditches
                dummyDitches = new ArrayList<ColoredDitch>();
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, second)));
                dummyDitches.add(allDitches.get(ColoredDitch.hashCode(second, third)));
                if (row > 1) {
                    partner = new Position(col + 1, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col, row - 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                }
                if (col > 1) {
                    partner = new Position(col - 1, row);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    partner = new Position(col - 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(first, partner)));
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col - 1, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                if (col + row < boardSize + 1) {
                    partner = new Position(col + 1, row + 1);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                    partner = new Position(col, row + 2);
                    dummyDitches.add(allDitches.get(ColoredDitch.hashCode(third, partner)));
                }
                currentDitch.setDitchNeighbors(dummyDitches);

                dummyFlowers = new ArrayList<ColoredFlower>();
                dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, second, third)));
                if (col > 1) {
                    partner = new Position(col - 1, row + 1);
                    dummyFlowers.add(allFlowers.get(ColoredFlower.hashCode(first, third, partner)));
                }
                currentDitch.setFlowerEdgeNeighbors(dummyFlowers);

                // node neighboring flowers
                dummyFlowersOne = new ArrayList<ColoredFlower>();
                dummyFlowersTwo = new ArrayList<ColoredFlower>();
                if (row > 1) {
                    partnerOne = new Position(col + 1, row - 1);
                    partnerTwo = new Position(col, row - 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col + 1, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                }
                if (col + row < boardSize + 1) {
                    partnerOne = new Position(col + 1, row + 1);
                    partnerTwo = new Position(col + 1, row);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                }
                if (col > 1 && col + row < boardSize + 1) {
                    partnerOne = new Position(col - 1, row + 2);
                    partnerTwo = new Position(col, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                }
                if (col > 1) {
                    partnerOne = new Position(col - 1, row + 1);
                    partnerTwo = new Position(col - 1, row + 2);
                    dummyFlowersTwo.add(allFlowers.get(ColoredFlower.hashCode(third, partnerOne, partnerTwo)));
                    partnerTwo = new Position(col - 1, row);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                }
                if (col > 1 && row > 1) {
                    partnerOne = new Position(col - 1, row);
                    partnerTwo = new Position(col, row - 1);
                    dummyFlowersOne.add(allFlowers.get(ColoredFlower.hashCode(first, partnerOne, partnerTwo)));
                }
                currentDitch.setFlowerNodeNeighborsOne(dummyFlowersOne);
                currentDitch.setFlowerNodeNeighborsTwo(dummyFlowersTwo);
            }
        }
    }
}
