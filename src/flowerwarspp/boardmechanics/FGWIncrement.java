package flowerwarspp.boardmechanics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
* <p>
* An FGWIncrement object consists of the difference between two FGWBoards.
* As a consequence, if the artificial intelligence runs a minmax algorithm on
* a tree on a given FGWBoard where each node consists of a 'Move'- Object , one doesn't need to copy
* the whole FGWBoard for each node. Each new (and old) FGWBoard state can be obtained
* by applying some 'make' or 'unmake' methods.
* <p>
* Let increment1 and increment2 be two objects of the class 'FGWIncrement' and let
* myBoard be a 'FGWBoard'. Then the state of myBoard is not changed by
* <ul>
* <li> increment1.make();
* <li> increment2.make();
* <li> increment2.unmake();
* <li> increment1.unmake();
* </ul>
* but differs after
* <ul>
* <li> increment1.make();
* <li> increment2.make();
* <li> increment1.unmake();
* <li> increment2.unmake();
* </ul>
* i.e. 'make' and 'unmake' methods of different FGWIncrement objects do not
* commute! <p>
*
* Generally speaking, an FGWIncrement object consists of all objects 'ColoredFlower', 'ColoredDitch' and 'FlowerSet'
* which have changed in being 'possible' or 'colored' between two 'FGWBoard's.
*
* An 'FGWIncrement' object is only used by objects of class 'PossibleMoveFlower'/'PossibleMoveDitch'.
* @version 4.2
* @author Martin Heide
*/
public class FGWIncrement {
    /** Contains a reference to the board.*/
    private FGWBoard board;
    /////////////// Flowers
    /** Contains a reference to the red possible flowers of the board.*/
    private HashMap<Integer, ColoredFlower> redPossibleFlowers;
    /** Contains the collection of incremental red flowers. */
    private Collection<ColoredFlower> excludeRedPossibleFlowers;
    /** Contains a reference to the blue possible flowers of the board.*/
    private HashMap<Integer, ColoredFlower> bluePossibleFlowers;
    /** Contains the collection of incremental blue flowers. */
    private Collection<ColoredFlower> excludeBluePossibleFlowers;
    /** Contains a reference to the red flowers of the board. */
    private Collection<FlowerSet> redFlowers;
    /** Contains a reference to the blue flowers of the board. */
    private Collection<FlowerSet> blueFlowers;
    /** Contains the flower set to be added in this increment. */
    private FlowerSet addFlowerSet;
    /** Contains the flower sets to be removed in this increment. */
    private Collection<FlowerSet> excludeFlowerSets;
    /** Contains the new flower to be added. */
    private ColoredFlower newColoredFlower;
    /** Contains the color of the new flower. */
    private FGWType typeOfNewColoredFlower;
    /////////////// Ditches
    /** Contains a reference to the red possible ditches of the board.*/
    private HashMap<Integer, ColoredDitch> redPossibleDitches;
    /** Contains the collection of red possible ditches which have to be excluded. */
    private Collection<ColoredDitch> excludeRedPossibleDitches;
    /** Contains the collection of red possible ditches which have to be added. */
    private Collection<ColoredDitch> addRedPossibleDitches;
    /** Contains a reference to the blue possible ditches of the board.*/
    private HashMap<Integer, ColoredDitch> bluePossibleDitches;
    /** Contains the collection of blue possible ditches which have to be excluded. */
    private Collection<ColoredDitch> excludeBluePossibleDitches;
    /** Contains the collection of blue possible ditches which have to be added. */
    private Collection<ColoredDitch> addBluePossibleDitches;
    /** Contains the collection of ditches which have to be set to fallow and are not
    * contained in any possible list. */
    private Collection<ColoredDitch> addFallowDitches;
    /** Contains the collection of ditches which have to be set to fallow and are
    * contained in red possible ditches. */
    private Collection<ColoredDitch> addFallowAndInPossibleRedDitch;
    /** Contains the collection of ditches which have to be set to fallow and are
    * contained in blue possible ditches. */
    private Collection<ColoredDitch> addFallowAndInPossibleBlueDitch;
    /** Contains a reference to the red ditches of the board.*/
    private Collection<ColoredDitch> redDitches;
    /** Contains a reference to the blue ditches of the board.*/
    private Collection<ColoredDitch> blueDitches;
    /** Contains the ditch which has to be added to the collection of ditches with the same color.*/
    private ColoredDitch addDitch;
    /** Contains the  node neighboring flower sets on one side of the ditch. */
    private Collection<FlowerSet> flowerSetsOfFirstDitchNodeNeighbors;
    /** Contains the  node neighboring flower sets on the other side of the ditch. */
    private Collection<FlowerSet> flowerSetsOfSecondDitchNodeNeighbors;
    /** Contains the color of the new ditch. */
    private FGWType ditchType;


    /**
    * Constructor which initializes the object with a reference to the board.
    * @param board FGWBoard on which the class works.
    */
    public FGWIncrement(FGWBoard board) {
        this.board = board;
        redPossibleFlowers = board.getRedPossibleFlowers();
        bluePossibleFlowers = board.getBluePossibleFlowers();
        blueFlowers = board.getBlueFlowers();
        redFlowers = board.getRedFlowers();
        redPossibleDitches = board.getRedPossibleDitches();
        bluePossibleDitches = board.getBluePossibleDitches();
        blueDitches = board.getBlueDitches();
        redDitches = board.getRedDitches();

        excludeRedPossibleFlowers = new ArrayList<ColoredFlower>();
        excludeBluePossibleFlowers = new ArrayList<ColoredFlower>();
        excludeRedPossibleDitches = new ArrayList<ColoredDitch>();
        addRedPossibleDitches = new ArrayList<ColoredDitch>();
        excludeBluePossibleDitches = new ArrayList<ColoredDitch>();
        addBluePossibleDitches = new ArrayList<ColoredDitch>();
        addFallowDitches = new ArrayList<ColoredDitch>();
        addFallowAndInPossibleRedDitch = new ArrayList<ColoredDitch>();
        addFallowAndInPossibleBlueDitch = new ArrayList<ColoredDitch>();
        excludeFlowerSets = new ArrayList<FlowerSet>();
        flowerSetsOfFirstDitchNodeNeighbors = new ArrayList<FlowerSet>();
        flowerSetsOfSecondDitchNodeNeighbors = new ArrayList<FlowerSet>();
    }

    // Public Methods Regarding Flowers
    /**
    * Contains a collection of FlowerSets which have to be deleted from the collection
    * of red/blue flowers.
    * Since information about the color of the FlowerSet are stored in the
    * FlowerSet, there exists only one method for both colors.
    * @param collectionOfFlowerSets Collection<FlowerSet>.
    */
    public void excludeTheFlowerSet(Collection<FlowerSet> collectionOfFlowerSets) {
        excludeFlowerSets.addAll(collectionOfFlowerSets);
    }

    /**
    * Same as excludeTheFlowerSet, overloaded function.
    * @param flowerSet FlowerSet.
    */
    public void excludeTheFlowerSet(FlowerSet flowerSet) {
        excludeFlowerSets.add(flowerSet);
    }

    /**
    * Contains the FlowerSet which has to be added to the collection red/blue flowers.
    * Since information about the color of the FlowerSet are stored in the
    * FlowerSet, there exists only one method for both colors.
    * @param flowerSet FlowerSet.
    */
    public void addTheFlowerSet(FlowerSet flowerSet) {
        addFlowerSet = flowerSet;
    }

    /**
    * Sets a new ColoredFlower. If the ColoredFlower Object in the argument is contained in a hashMap
    * <col>possibleFlowers, it will be added to exclude<col>PossibleFlowers. Since there exists no functionality which excludes a flower from these HashMaps if a ColoredFlower was already set, this method can only be called one time!
    * @param flower argument of Type ColoredFlower which has to be set in the increment.
    */
    public void setTheNewColoredFlower(ColoredFlower flower) {
        if (newColoredFlower instanceof ColoredFlower) {
            throw new IllegalArgumentException("This function can only be called one time!");
        }
        newColoredFlower = flower;
        // check if the flower is in one or two possible<col>Flowers HashMaps and exclude the flower from the HashMaps if true.
        if (!redPossibleFlowers.containsValue(flower) && !bluePossibleFlowers.containsValue(flower)) {
            throw new IllegalArgumentException("The flower in the Argument is not possible for both colors!");
        }
        if (redPossibleFlowers.containsValue(flower) && !excludeRedPossibleFlowers.contains(flower)) {
            excludeRedPossibleFlowers.add(flower);
        }
        if (bluePossibleFlowers.containsValue(flower) && !excludeBluePossibleFlowers.contains(flower)) {
            excludeBluePossibleFlowers.add(flower);
        }
    }

    /**
    * Sets the FGWType of the ColoredFlower Object which has to be set.
    * @param type Type of the new ColoredFlower.
    */
    public void setTypeOfNewColoredFlower(FGWType type) {
        typeOfNewColoredFlower = type;
    }

    /**
    * Adds a red ColoredFlower object to the collection of elements to be excluded
    * from the HashMap of possible red flowers.
    * @param flower ColoredFlower to be excluded.
    */
    public void excludeTheRedPossibleFlower(ColoredFlower flower) {
        if (redPossibleFlowers.containsValue(flower) && !excludeRedPossibleFlowers.contains(flower)) {
            excludeRedPossibleFlowers.add(flower);
        }
    }

    /**
    * Same as excludeRedPossibleFlowers, overloaded function.
    * Allows a collection of ColoredFlowers to be excluded from the collection of blue possible flowers.
    * @param flowers Collection<ColoredFlower>.
    */
    public void excludeTheRedPossibleFlower(Collection<ColoredFlower> flowers) {
        for (ColoredFlower cf : flowers) {
            excludeTheRedPossibleFlower(cf);
        }
    }

    /**
    * Adds a blue ColoredFlower element to the collection of elements to be excluded.
    * @param flower ColoredFlower to be excluded.
    */
    public void excludeTheBluePossibleFlower(ColoredFlower flower) {
        if (bluePossibleFlowers.containsValue(flower) && !excludeBluePossibleFlowers.contains(flower)) {
            excludeBluePossibleFlowers.add(flower);
        }
    }

    /** Same as excludeTheBluePossibleFlower(ColoredFlower flower) for a collection
    * @param flowers Collection<ColoredFlower>.
    */
    public void excludeTheBluePossibleFlower(Collection<ColoredFlower> flowers) {
        for (ColoredFlower cf : flowers) {
            excludeTheBluePossibleFlower(cf);
        }
    }

    /**
    * Checks if the argument is contained in the collection of red possible flowers.
    * @param flower ColoredFlower.
    * @return True, if flower is contained in the collection.
    */
    public boolean excludeRedPossibleFlowersContains(ColoredFlower flower) {
        return excludeRedPossibleFlowers.contains(flower);
    }

    /**
    * Checks if the argument is contained in the collection of blue possible flowers.
    * @param flower ColoredFlower.
    * @return bTrue, if flower is contained in the collection.
    */
    public boolean excludeBluePossibleFlowersContains(ColoredFlower flower) {
        return excludeBluePossibleFlowers.contains(flower);
    }

    // Public Methods Regarding Ditches
    /**
    * Returns the ColoredDitch added in this increment.
    * @return The new ColoredDitch.
    */
    public ColoredDitch getIncrementalColoredDitch() {
        if (addDitch instanceof ColoredDitch) {
            return addDitch;
        } else {
            return null;
        }
    }

    /**
    * Adds the ditch of the argument to the possible ditches of the specified color.
    * @param ditch The ColoredDitch to add.
    * @param type The color of this new ColoredDitch.
    */
    public void addToPossibleDitches(ColoredDitch ditch, FGWType type) {
        if (type == FGWType.BLUE) {
            if (!addBluePossibleDitches.contains(ditch) && !bluePossibleDitches.containsValue(ditch)) {
                addBluePossibleDitches.add(ditch);
            }
        } else if (type == FGWType.RED) {
            if (!addRedPossibleDitches.contains(ditch) && !redPossibleDitches.containsValue(ditch)) {
                addRedPossibleDitches.add(ditch);
            }
        }
    }

    /**
    * Adds the argument to the collection of ditches which have to be set to
    * fallow.
    * If the ditch is contained in the HashMap red/blue possible ditches,
    * the ditch will be stored in a seperate collection in order to unmake the
    * step.
    * @param ditch ColoredDitch.
    * @return boolean returns false if the argument ditch is FGWType.FALLOW.
    */
    public boolean setDitchToFallow(ColoredDitch ditch) {
        if (ditch.getDitchType() == FGWType.FALLOW) {
            return false;
        }
        // ditch in possible red or (no Ex-Or) blue ditches?

        if (redPossibleDitches.containsValue(ditch)) {
            addFallowAndInPossibleRedDitch.add(ditch);
        }
        if (bluePossibleDitches.containsValue(ditch)) {
            addFallowAndInPossibleBlueDitch.add(ditch);
        }
        if (!redPossibleDitches.containsValue(ditch) && !bluePossibleDitches.containsValue(ditch)) {
            addFallowDitches.add(ditch);
        }
        return true;
    }

    /**
    * Adds the argument collection to the collection of ditches which have to be set to
    * fallow.
    * If the collection is contained in the HashMap red/blue possible ditches,
    * the ditch will be stored in a seperate collection in order to unmake the
    * step.
    * @param ditches Collection<ColoredDitch> which has to be set to fallow.
    */
    public void setDitchToFallow(Collection<ColoredDitch> ditches) {
        for (ColoredDitch cd : ditches) {
            setDitchToFallow(cd);
        }
    }

    /**
    * Contains the argument in a variable which will be added to the collection
    * of ditches of the particular color/Type.
    * @param ditch ColoredDitch.
    * @param type FGWType of the ditch argument.
    */
    public void addToDitches(ColoredDitch ditch, FGWType type) {
        if (addDitch instanceof ColoredDitch) {
            throw new IllegalArgumentException("The method 'addToDitch' can only be called one time!");
        }
        if (type == FGWType.RED) {
            if (!redPossibleDitches.containsValue(ditch)) {
                throw new IllegalArgumentException("Only red ditches which are contained in the collection of possibleRedDitches can be set!");
            }
        } else if (type == FGWType.BLUE) {
            if (!bluePossibleDitches.containsValue(ditch)) {
                throw new IllegalArgumentException("Only  blue ditches which are contained in the collection of possibleBlueDitches can be set!");
            }
        } else {
            throw new IllegalArgumentException("Only ditches with type 'BLUE' or 'RED' can be added to the particular Collection! ");
        }
        addDitch = ditch;
        ditchType = type;
        // Set ditchNeighbors of the ditch to fallow!
        setDitchToFallow(ditch.getDitchNeighbors());
        // Collects the information about the FlowerSets in the edgeNeighborhoods
        // of the given ditch.
        for (ColoredFlower cf : ditch.getFlowerNodeNeighborsOne()) {
            if (cf.getFlowerType() == ditchType) {
                if (!flowerSetsOfFirstDitchNodeNeighbors.contains(cf.getParentFlowerSet())) {
                    flowerSetsOfFirstDitchNodeNeighbors.add(cf.getParentFlowerSet());
                }
            }
        }
        for (ColoredFlower cf : ditch.getFlowerNodeNeighborsTwo()) {
            if (cf.getFlowerType() == ditchType) {
                if (!flowerSetsOfSecondDitchNodeNeighbors.contains(cf.getParentFlowerSet())) {
                    flowerSetsOfSecondDitchNodeNeighbors.add(cf.getParentFlowerSet());
                }
            }
        }
        if (redPossibleDitches.containsValue(ditch)) {
            excludeRedPossibleDitches.add(ditch);
        }
        if (bluePossibleDitches.containsValue(ditch)) {
            excludeBluePossibleDitches.add(ditch);
        }

    }

    /**
    * Realizes a 'forward' step based on the increments. All collections of Flowers,
    * Flowersets and Ditches (and possible, respectively), which have to be set based on the increments, will be set.
    * An increment only 'makes' a ditch move, if the class variable 'addDitch' was set.
    * An increment only 'makes' the first or second increment of a flower move, if a FlowerSet 'addFlowerSet' was set.
    */
    public void make() {
        makePossibleFlowers();
        if (addFlowerSet instanceof FlowerSet) {
            makeFlowers();
        }
        makePossibleDitches();
        if (addDitch instanceof ColoredDitch) {
            makeDitch(addDitch);
        }

    }

    /**
    * Realizes a 'backward' step based on the increments.
    * The unmake step assumes that all increment collections of Flowers, Flowersets and
    * Ditches are already set. The increment will be undone.
    */
    public void unmake() {
        unmakePossibleFlowers();
        if (addFlowerSet instanceof FlowerSet) {
            unmakeFlowers();
        }
        unmakePossibleDitches();
        if (addDitch instanceof ColoredDitch) {
            unmakeDitch(addDitch);
        }
    }

    /**
    * If the class variable addFlowerSet was set by 'setTheNewColoredFlower', the addFlowerSet
    * will be added to the newColoredFlower.
    * The new ColoredFlower gets its FGWType.
    * All ColoredFlowers in the Collection 'excludeFlowerSets' will get the new parentFlowerSet reference as the addFlowerSet Variable.
    * All excludeFlowerSets FlowerSet will be removed from the Collection of redFlowers/blueFlowers
    * The new FlowerSet 'addFlowerSet' will be added to redFlowers/blueFlowers
    */
    private void makeFlowers() {
        newColoredFlower.setParentFlowerSet(addFlowerSet);
        newColoredFlower.setFlowerType(typeOfNewColoredFlower);
        for (FlowerSet fs : excludeFlowerSets) {
            for (ColoredFlower cf : fs.getFlowers()) {
                cf.setParentFlowerSet(addFlowerSet);
            }
            // add the collection ditchConnectedFlowerSets of fs to the ditchConnectedFlowerSets of the addFlowerSet flowerSet
            addFlowerSet.addToDitchConnectedFlowerSet(fs.getDitchConnectedFlowerSet());
            // set the references of the ditch-connected flowersets of each fs to the new one!
            if (fs.getDitchConnectedFlowerSet().size() > 0) {
                for (FlowerSet dconFS : fs.getDitchConnectedFlowerSet()) {
                    Collection<FlowerSet> ditchConnFS = dconFS.getDitchConnectedFlowerSet();
                    Iterator<FlowerSet> iterator = ditchConnFS.iterator();
                    Integer countDeletions = 0;
                    while (iterator.hasNext()) {
                        if (iterator.next() == fs) {
                            iterator.remove();
                            countDeletions++;
                        }
                    }
                    for (int i = 0; i < countDeletions; i++) {
                        ditchConnFS.add(addFlowerSet);
                    }
                }
            }
        }
        // avoids that viewer and this method try to access the same redFlowers/blueFlowers methodes at the same time.
        synchronized (board) {
            if (typeOfNewColoredFlower == FGWType.RED) {
                redFlowers.removeAll(excludeFlowerSets);
                redFlowers.add(addFlowerSet);
            } else if (typeOfNewColoredFlower == FGWType.BLUE) {
                blueFlowers.removeAll(excludeFlowerSets);
                blueFlowers.add(addFlowerSet);
            }
        }
    }

    /**
    * 'Unmake' step for an increment of a flower move. The new flower will be set to
    * uncolored and it will loose its parentFlowerSet. Its parentFlowerSet
    * will be deleted from the list of <color>Flowers and all ColoredFlowers contained
    * in the parentFlowerSet will get back their old parentFlowerSet.
    * The reference on the parentFlowerSet in each other ditch-connected FlowerSet
    * will be set back to the FlowerSets in the Collection excludeFlowerSets.
    */
    private void unmakeFlowers() {
        newColoredFlower.setParentFlowerSet(null);
        newColoredFlower.setFlowerType(FGWType.UNCOLORED);
        for (FlowerSet fs : excludeFlowerSets) {
            for (ColoredFlower cf : fs.getFlowers()) {
                cf.setParentFlowerSet(fs);
            }
            // set the references of the ditch-connected flowersets of each fs to the old one!
            if (fs.getDitchConnectedFlowerSet().size() > 0) {
                for (FlowerSet dconFS : fs.getDitchConnectedFlowerSet()) {
                    Collection<FlowerSet> ditchConnFS = dconFS.getDitchConnectedFlowerSet();
                    Iterator<FlowerSet> iterator = ditchConnFS.iterator();
                    Integer countDeletions = 0;
                    while (iterator.hasNext()) {
                        if (iterator.next() == addFlowerSet) {
                            iterator.remove();
                            countDeletions++;
                        }
                    }
                    for (int i = 0; i < countDeletions; i++) {
                        ditchConnFS.add(fs);
                    }
                }
            }
        }
        synchronized (board) {
            if (typeOfNewColoredFlower == FGWType.RED) {
                redFlowers.addAll(excludeFlowerSets);
                redFlowers.remove(addFlowerSet);
            } else if (typeOfNewColoredFlower == FGWType.BLUE) {
                blueFlowers.addAll(excludeFlowerSets);
                blueFlowers.remove(addFlowerSet);
            }
        }
    }

    /**
    * Realizes forward changes of the red and blue possible Flowers HashMap.
    * Each element in the Collection of 'exclude<col>PossibleFlowers' will be removed from the HashMap of '<col>PossibleFlowers'.
    */
    private void makePossibleFlowers() {
        synchronized (board) {
            for (ColoredFlower cf : excludeRedPossibleFlowers) {
                if (!redPossibleFlowers.containsKey(cf.hashCode())) {
                    throw new NoSuchElementException("Fehler in makePossibleFlowers RED Methode");
                }
                redPossibleFlowers.remove(cf.hashCode());
            }
            for (ColoredFlower cf : excludeBluePossibleFlowers) {
                if (!bluePossibleFlowers.containsKey(cf.hashCode())) {
                    throw new NoSuchElementException("Fehler in makePossibleFlowers BLUE Methode");
                }
                bluePossibleFlowers.remove(cf.hashCode());
            }
        }
    }

    /**
    * Realizes backward changes of the red and blue possible Flowers HashMap.
    * Each element in the Collection of 'exclude<col>PossibleFlowers' will be added to the HashMap of '<col>PossibleFlowers'.
    */
    private void unmakePossibleFlowers() {
        synchronized (board) {
            for (ColoredFlower cf : excludeRedPossibleFlowers) {
                if (redPossibleFlowers.containsKey(cf.hashCode())) {
                    throw new IllegalArgumentException("Fehler in unmakePossibleFlowers: RedFlower kann nicht hinzugefuegt werden, ist schon vorhanden!");
                }
                redPossibleFlowers.put(cf.hashCode(),cf);
            }
            for (ColoredFlower cf : excludeBluePossibleFlowers) {
                if (bluePossibleFlowers.containsKey(cf.hashCode())) {
                    throw new IllegalArgumentException("Fehler in unmakePossibleFlowers: BlueFlower kann nicht hinzugefuegt werden, ist schon vorhanden!");
                }
                bluePossibleFlowers.put(cf.hashCode(),cf);
            }
        }
    }

    /**
    * Realizes forward changes of the red and blue possible Ditches HashMap.
    * The method does the Following for Colors <col> RED and BLUE:
    * - Adds the collection 'add<col>PossibleDitches' to HashMap of <col>PossibleDitches. <br>
    * - Excludes the collection 'exclude<col>PossibleDitches' from HashMap of <col>PossibleDitches. <br>
    * - Excludes the collection 'addFallowAndInPossibleRedDitch' from HashMap of <col>PossibleDitches. <br>
    * - Excludes the collection 'addFallowAndInPossibleBlueDitch' from HashMap of <col>PossibleDitches. <br>
    * - Sets the collection 'addFallowDitches' to Fallow.
    */
    private void makePossibleDitches() {
        makeAddAndExcludePossibleDitches(redPossibleDitches,excludeRedPossibleDitches,addRedPossibleDitches);
        makeExcludeFallowsFromPossibleDitches(redPossibleDitches,addFallowAndInPossibleRedDitch);
        makeAddAndExcludePossibleDitches(bluePossibleDitches,excludeBluePossibleDitches,addBluePossibleDitches);
        makeExcludeFallowsFromPossibleDitches(bluePossibleDitches,addFallowAndInPossibleBlueDitch);
        for (ColoredDitch cd : addFallowDitches) {
            cd.setDitchType(FGWType.FALLOW);
        }
    }

    /** Function that offers functionality for collections/ HashMaps of red and
    * blue ditches specified by the arguments.
    * The arguments are '<col>PossibleDitches', 'exclude<col>PossibleDitches', 'add<col>PossibleDitches'.
    * The collection of 'add<col>PossibleDitches' is added to '<col>PossibleDitches'
    * and the collection of 'exclude<col>PossibleDitches' is excluded from '<col>PossibleDitches'.
    * @param possibleDitchesHashMap HashMap<Integer, ColoredDitch> of the possible ditches of a color.
    * @param excludeDitches Collection of Ditches which has to excluded from <col>PossibleDitches.
    * @param addDitches Collection of Ditches which has to be added to <col>PossibleDitches.
    */
    private void makeAddAndExcludePossibleDitches(HashMap<Integer, ColoredDitch> possibleDitchesHashMap,
                                                  Collection<ColoredDitch> excludeDitches,
                                                  Collection<ColoredDitch> addDitches) {
        for (ColoredDitch cd : addDitches) {
            if (possibleDitchesHashMap.containsKey(cd.hashCode())) {
                throw new IllegalArgumentException("Error in makeAddExcludePossibleDitches: ditch is already contained in the hashMap of possibleDitches!");
            }
            possibleDitchesHashMap.put(cd.hashCode(),cd);
        }
        for (ColoredDitch cd : excludeDitches) {
            if (!possibleDitchesHashMap.containsKey(cd.hashCode())) {
                throw new IllegalArgumentException("Error in makeAddExcludePossibleDitches: ditch can not be removed because it is not contained in the hashMap of possibleDitches!");
            }
            possibleDitchesHashMap.remove(cd.hashCode());
        }
    }

    /** Excludes all elements from the collection 'addFallowAndInPossible<col>Ditch' which is passed in the second arguments from the HashMap '<col>PossibleDitches' which is passed in the first argument.
    * Sets all of the '<col>PossibleDitches' to FALLOW. <br>
    * This function 'makes' the functionality (compare to 'makeExcludeFallowsFromPossibleDitches').
    * @param possibleDitchesHashMap HashMap<Integer, ColoredDitch> which ontains the possible ditches of a given color <col>.
    * @param addFallowAndInPossibleDitch Collection<ColoredDitch> Contains the ditches which have to be unset from FALLOW to UNCOLORED and added to the HashMap of possible ditches of the given color.
    */
    private void makeExcludeFallowsFromPossibleDitches(HashMap<Integer, ColoredDitch> possibleDitchesHashMap, Collection<ColoredDitch> addFallowAndInPossibleDitch) {
        for (ColoredDitch cd : addFallowAndInPossibleDitch) {
            possibleDitchesHashMap.remove(cd.hashCode());
            cd.setDitchType(FGWType.FALLOW);
        }
    }

    /**
    * Realizes backward changes of the red and blue possible ditches hash map.
    * The method does the following for colors <col> RED and BLUE: <br>
    * - Adds the collection 'exclude<col>PossibleDitches' to HashMap of <col>PossibleDitches. <br>
    * - Excludes the collection 'add<col>PossibleDitches' from HashMap of <col>PossibleDitches. <br>
    * - Adds the collection 'addFallowAndInPossibleRedDitch' to HashMap of <col>PossibleDitches
    * and sets them to UNCOLORED. <br>
    * - Adds the collection 'addFallowAndInPossibleBlueDitch' to HashMap of <col>PossibleDitches
    * and sets them to UNCOLORED. <br>
    * - Sets the collection 'addFallowDitches' to UNCOLORED.
    */
    private void unmakePossibleDitches() {
        unmakeAddAndExcludePossibleDitches(redPossibleDitches,excludeRedPossibleDitches,addRedPossibleDitches);
        unmakeExcludeFallowsFromPossibleDitches(redPossibleDitches,addFallowAndInPossibleRedDitch);
        unmakeAddAndExcludePossibleDitches(bluePossibleDitches,excludeBluePossibleDitches,addBluePossibleDitches);
        unmakeExcludeFallowsFromPossibleDitches(bluePossibleDitches,addFallowAndInPossibleBlueDitch);
        for (ColoredDitch cd : addFallowDitches) {
            cd.setDitchType(FGWType.UNCOLORED);
        }
    }

    /**
    * Function that offers functionality for collections/ hash maps of red and
    * blue ditches specified by the arguments.
    * The arguments are '<col>PossibleDitches', 'exclude<col>PossibleDitches', 'add<col>PossibleDitches'.
    * The collection of 'exclude<col>PossibleDitches' is added to '<col>PossibleDitches'
    * and the collection of 'add<col>PossibleDitches' is excluded from '<col>PossibleDitches'.
    * @param possibleDitchesHashMap HashMap<Integer, ColoredDitch> of the possible ditches of a color.
    * @param excludeDitches Collection of Ditches which has to be added to <col>PossibleDitches.
    * @param addDitches Collection of Ditches which has to be excluded from <col>PossibleDitches.
    */
    private void unmakeAddAndExcludePossibleDitches(HashMap<Integer, ColoredDitch> possibleDitchesHashMap,
                                                    Collection<ColoredDitch> excludeDitches,
                                                    Collection<ColoredDitch> addDitches) {
        for (ColoredDitch cd : excludeDitches) {
            if (possibleDitchesHashMap.containsKey(cd.hashCode())) {
                throw new IllegalArgumentException(" Error in unmakeAddAndExcludePossibleDitches: ditch cannot be added from excludeCollection to HashMap because it is already contained in the HashMap of possibleDitches!");
            }
            possibleDitchesHashMap.put(cd.hashCode(),cd);
        }
        for (ColoredDitch cd : addDitches) {
            if (!possibleDitchesHashMap.containsKey(cd.hashCode())) {
                throw new IllegalArgumentException(" Error in unmakeAddAndExcludePossibleDitches: ditch cannot be removed because it is not contained in the HashMap of possibleDitches!");
            }
            possibleDitchesHashMap.remove(cd.hashCode());
        }
    }

    /**
    * Adds all elements from the collection 'addFallowAndInPossible<col>Ditch' which is passed in the second argument to the hash map '<col>PossibleDitches' which is passed in the first argument.
    * This function 'unmakes' the functionality of 'makeExcludeFallowsFromPossibleDitches'.
    * Sets all ditches in '<col>PossibleDitches' to UNCOLORED.
    * @param possibleDitchesHashMap HashMap<Integer, ColoredDitch> which contains the possible ditches of a given color <col>.
    * @param addFallowAndInPossibleDitch Collection<ColoredDitch> contains the ditches which have to be unset from FALLOW to UNCOLORED and added to the hash map of possible ditches of the given color.
    */
    private void unmakeExcludeFallowsFromPossibleDitches(HashMap<Integer, ColoredDitch> possibleDitchesHashMap, Collection<ColoredDitch> addFallowAndInPossibleDitch) {
        for (ColoredDitch cd : addFallowAndInPossibleDitch) {
            possibleDitchesHashMap.put(cd.hashCode(),cd);
            cd.setDitchType(FGWType.UNCOLORED);
        }
    }

    /**
    * Adds the ditch argument to the ditches collection.
    * Sets the references in neighboring FlowerSets which are connected to each other
    * by the ditch. If two FlowerSets are connected by more than one ditch, the
    * references to each other will occur as often as connecting ditches exist.
    * @param ditch ColoredDitch which was set.
    */
    private void makeDitch(ColoredDitch ditch) {
        if (ditchType != FGWType.RED && ditchType != FGWType.BLUE) {
            throw new IllegalArgumentException("The 'makeAddDitch' method got an argument with type value unequal to red and unequal to blue!");
        }
        // Add References between each ditch-connected FlowerSet
        for (FlowerSet fs : flowerSetsOfFirstDitchNodeNeighbors) {
            fs.getDitchConnectedFlowerSet().addAll(flowerSetsOfSecondDitchNodeNeighbors);
        }
        for (FlowerSet fs : flowerSetsOfSecondDitchNodeNeighbors) {
            fs.getDitchConnectedFlowerSet().addAll(flowerSetsOfFirstDitchNodeNeighbors);
        }
        synchronized (board) {
            if (ditchType == FGWType.RED) {
                redDitches.add(ditch);
            } else if (ditchType == FGWType.BLUE) {
                blueDitches.add(ditch);
            }
            ditch.setDitchType(ditchType);
        }
    }

    /**
    * Removes the ditch argument from the ditches collection.
    * Deletes references in neighboring FlowerSets to neighboring Flowersets of
    * the other neighborhood (which are connected to each other)
    * by the ditch. While one FlowerSet reference may occur multiple times in
    * another FlowerSet, only one reference is deleted.
    * @param ditch ColoredDitch which was set.
    */
    private void unmakeDitch(ColoredDitch ditch) {
        if (ditchType != FGWType.RED && ditchType != FGWType.BLUE) {
            throw new IllegalArgumentException("The 'makeAddDitch' method got an argument with type value unequal to red and unequal to blue!");
        }
        // Remove ONE Reference between all ditch-connected FlowerSet
        for (FlowerSet fsFirst : flowerSetsOfFirstDitchNodeNeighbors) {
            for (FlowerSet fsSecond : flowerSetsOfSecondDitchNodeNeighbors) {
                fsFirst.getDitchConnectedFlowerSet().remove(fsSecond);
                fsSecond.getDitchConnectedFlowerSet().remove(fsFirst);
            }
        }
        synchronized (board) {
            if (ditchType == FGWType.RED) {
                redDitches.remove(ditch);
            } else if (ditchType == FGWType.BLUE) {
                blueDitches.remove(ditch);
            }
            ditch.setDitchType(FGWType.UNCOLORED);
        }
    }
}
