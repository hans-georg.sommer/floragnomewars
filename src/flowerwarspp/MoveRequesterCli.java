package flowerwarspp;

import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveFormatException;
import flowerwarspp.preset.Requestable;
import flowerwarspp.preset.Viewer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Simple input class which reads moves from the command line.
 *
 * @author Hans-Georg Sommer
 */
public class MoveRequesterCli implements Requestable {
    /**
     * Viewer used to determine the color of the current player.
     */
    private Viewer viewer;
    /**
     * Reader used to read input from stdin.
     */
    private BufferedReader input;

    /**
     * Create a new MoveRequesterCli.
     *
     * @param viewer
     *         Viewer to check the current turn.
     */
    public MoveRequesterCli(Viewer viewer) {
        this.viewer = viewer;
        input = new BufferedReader(new InputStreamReader(System.in));
    }

    /*
     * @see flowerwars.preset.Requestable#request(flowerwars.preset.Requestable)
     */
    @Override
    public Move request() {
        Move move = null;
         do {
            try {
                System.out.printf("Enter move for player %s: ", viewer.getTurn().name());
                move = Move.parseMove(input.readLine());
            } catch (IOException | MoveFormatException e) {
                System.out.println("Error: " + e);
            }
        } while (move == null);
        return move;
    }
}
