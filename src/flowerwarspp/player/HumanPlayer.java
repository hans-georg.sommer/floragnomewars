package flowerwarspp.player;

import flowerwarspp.preset.Move;
import flowerwarspp.preset.PlayerType;
import flowerwarspp.preset.Requestable;

/**
 * Class for an interactive human player that plays interactively via the board.
 * @author Felix Strnad
 */
public class HumanPlayer extends BasicPlayer {
    /** Input variable. */
    private Requestable input;

    /**
     * Default constructor of the Human Player.
     */
    public HumanPlayer() {
        this.type = PlayerType.HUMAN;
    }

    /**
     * Here we get the interactive input that the user chose.
     * @param input Input of the user.
     */
    public void setInputSource(Requestable input) {
        this.input = input;
    }

    /**
     * This function chooses the next move of a human player using the interactive input.
     * @see flowerwarspp.player.BasicPlayer#chooseMove()
     */
    @Override
    public Move chooseMove() throws Exception {
        nextMove = input.request();
        if (nextMove == null) {
            System.out.println("Please insert Move that is not null!");
            throw new Exception ("Move that Human Player wants to take is null!");
        }
        return nextMove;
    }
}
