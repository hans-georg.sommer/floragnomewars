package flowerwarspp.player;

import flowerwarspp.Debug;
import flowerwarspp.boardmechanics.FGWBoard;
import flowerwarspp.boardmechanics.FGWType;
import flowerwarspp.gui.BoardDisplay;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Player;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.PlayerType;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.Viewer;

/**
 * This is the implementation of a player class. It is implemented as an abstract class, such that
 * each different player (AI, human player, random player) can use the same functions but the way they
 * request draws will be different.
 * @author Felix Strnad
 */
public abstract class BasicPlayer implements Player {
    /** type of the player */
    protected PlayerType type;
    /** Status in which this player is.*/
    protected PlayerStatus status;
    /** The color of this player. */
    protected PlayerColor color;
    /** Needed variable for the submitting the next move of the underlying player. */
    protected Move nextMove;
    /** Each player has a copy of the board that controls the game. */
    protected FGWBoard myBoard;
    /** Each player needs a viewer to watch the game.*/
    protected Viewer viewer;
    /** Player needs a display for the network game. */
    protected BoardDisplay display;
    /** Needed for KI. It needs to know in which color the player's color is set. */
    protected FGWType myFlowerColor;

    /**
     * Creates a new player. It has later to be further initialized.
     */
    public BasicPlayer() {
        status = PlayerStatus.NOTEXISTING;
    }

    /**
     * Get the color of this player.
     * @return PlayerColor Color of this player.
     */
    public PlayerColor getColor() {
        return color;
    }

    /**
     * Sets the display of the board.
     * @param display The BoardDisplay of the game.
     */
    public void setDisplay(BoardDisplay display) {
        this.display = display;
    }

    /**
     * Abstract initialization of the player with the given color.
     * Creates a board of the given boardSize.
     * @param boardSize Size of the board.
     * @param color Player's color.
     * @throws Exception If called in the wrong order.
     * @see flowerwarspp.preset.Player#init(int, PlayerColor)
     */
    @Override
    public void init(final int boardSize, PlayerColor color) throws Exception {
        Debug.print("init player " + color);
        this.color = color;
        myBoard = new FGWBoard(boardSize);
        viewer = myBoard.viewer();

        if (status == PlayerStatus.INITIALIZED) {
            throw new Exception("The player seems to be already initialized!" + status);
        }

        if (this.color == PlayerColor.Red) {
            // Red Player calls first request()
            status = PlayerStatus.UPDATE;
        } else if (this.color == PlayerColor.Blue) {
            // Blue Player calls first update()
            status = PlayerStatus.CONFIRM;
        } else {
            System.out.println("Invalid color of the player, no status was set!");
        }

        String first, second;
        if (color == PlayerColor.Red) {
            myFlowerColor = FGWType.RED;
            first = "local";
            second = "remote";
        } else if (color == PlayerColor.Blue) {
            myFlowerColor = FGWType.BLUE;
            first = "remote";
            second = "local";
        } else {
            myFlowerColor = null;
            first = "unknown";
            second = "unknown";
            System.out.println("Something went wrong with identifiy in which color my flowers are!");
        }
        if (display != null) {
            Debug.print("Resize local board.");
            display.setViewer(viewer);
            display.setPlayerNames(first, second);
            display.reset();
        }
    }

    /**
     * Requests a new draw of the player.
     * @return The next move.
     * @throws Exception if the Status of the player is not the init() or update() status.
     * @see flowerwarspp.preset.Player#request()
     */
    @Override
    public Move request() throws Exception {
        Debug.print("Request Move! " + status + " " + color);
        if (status == PlayerStatus.NOTEXISTING) {
            throw new Exception("Call init() before using request()");
        }
        if (status == PlayerStatus.CONFIRM) { // Request might be called multiple times!
            throw new Exception("In the request() method the player must not be in CONFIRM status!" + color + " " + type);
        }
        nextMove = chooseMove();
        status = PlayerStatus.REQUEST;
        return nextMove;
    }

    /**
     * This function chooses a move depending on the underlying player.
     * Every different player implements this function differently based on the specific type he/she is.
     * @throws Exception if the returned move is null.
     * @return Next move to the Board.
     */
    public abstract Move chooseMove() throws Exception;

    /**
     * Confirms that own status is in agreement with the status on the MainBoard.
     * @param boardStatus Status of the board.
     * @throws Exception if the player is not in the request state, which has to be the request state.
     * @see flowerwarspp.preset.Player#confirm(Status)
     */
    @Override
    public void confirm(final Status boardStatus) throws Exception {
        if (status != PlayerStatus.REQUEST) {   //check the correct order. Red player starts with request().
            System.out.println("Wrong order of execution!" + color);
            throw new Exception("call request() before confirm()");
        }
        myBoard.make(nextMove);
        if (boardStatus != myBoard.getCurrentStatus()) {
            throw new Exception("Confusion! Board of Player not in the same state as Control Board!");
        }
        updateDisplay(nextMove, boardStatus);
        status = PlayerStatus.CONFIRM;
    }

    /**
     * Updates the state of the player board with the gameboard on which the actual game happens.
     * @param opponentMove Move of the opponent player.
     * @param boardStatus Status of the board.
     * @throws Exception if the player is in not in the confirm state and if the own board status is unequal the gameboard status.
     * @see flowerwarspp.preset.Player#update(Move, Status)
     */
    @Override
    public void update(final Move opponentMove, final Status boardStatus) throws Exception {
        if (status == PlayerStatus.NOTEXISTING) {
            throw new Exception("Call init() before using request()" + color);
        } else if (status == PlayerStatus.REQUEST) {
            throw new Exception("call confirm() before update()" + color);
        }
        myBoard.make(opponentMove);
        if (boardStatus != myBoard.getCurrentStatus()) {
            throw new Exception("confusion at boardstatus" + color);
        }
        updateDisplay(opponentMove, boardStatus);
        status = PlayerStatus.UPDATE;
    }

    /**
     * Updates the Display of the game after a move.
     * @param move The new move of the board.
     * @param status The new status of the board.
     */
    protected void updateDisplay(Move move, Status status) {
        if (display != null) {
            display.update(move, status);
        }
    }
}
