package flowerwarspp.player;

import java.util.Collection;

import flowerwarspp.Debug;
import flowerwarspp.gui.BoardDisplay;
import flowerwarspp.gui.FGWDisplay;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.PlayerType;
import visualtree.RedBlueTreeDrawer;

/**
 * Provides an intelligent player. (In the following also called AI). <br>
 * The strategy of the AI is to strongly reduce the amount of possible moves based on a
 * scoring of the different moves. First of all, FlowerSets are always completed to gardens.
 * Secondly, flower moves of two connected flowers which lie in a so called
 * "outer neighborhood" are considered.
 * If the number of possible moves is small enough, this strategy is combined with a
 * minmax algorithm.
 * @author Felix Strnad, Charlotte Ackva
 */
public class IntelligentPlayer extends BasicPlayer {

    /** The maximal score a move can get. */
    private static final int MAX_SCORE = 1000000;
    /** The minimal score a move can get. */
    private static final int MIN_SCORE = -1000000;
    /** The amount of possible moves where the AI starts using a tree to calculate moves in advance.*/
    private static final int MAX_MOVES_SIZE_FOR_TREE = 5000;

    /** The current tree depth (may vary during a game).*/
    private int currentTreeDepth = 0;
    /** The current tree width (may vary during a game).*/
    private int currentTreeWidth = 0;
    /** The root node where the tree starts. */
    private Node currentRootNode;
    /** The RedBlueTreeDrawer (if wanted). */
    private RedBlueTreeDrawer visual;

    /**
     * Constructs an intelligent player.
     */
    public IntelligentPlayer() {
        type = PlayerType.ADVANCED_AI_2;
        visual = new RedBlueTreeDrawer(false);
    }

    /**
     * Chooses the next move and delivers it to the request() function of the BasicPlayer. <br>
     * A tree is build from the current root node with the current tree depth/width and
     * the best move is chosen with that tree.
     * @return The next move the AI makes.
     * @throws Exception If returned move is null.
     * @see flowerwarspp.player.BasicPlayer#chooseMove
     */
    @Override
    public Move chooseMove() throws Exception {
        setTreeSize();
        currentRootNode = new Node(myBoard, color, currentTreeWidth);
        currentRootNode.buildTree(currentTreeDepth);

        visual.draw(currentRootNode);

        if (getBestMove() == null) {
            throw new Exception("The move the Intelligent player wants to make is null!");
        }
        return getBestMove();
    }

    /**
     * Chooses the move with the highest score attained via the minmax algorithm. <br>
     * Iterates over all children and determines their minmax values, chooses the best one.
     * @return The child with highest minmax value.
     */
    private Move getBestMove() {
        Collection<Node> childrenOfRoot = currentRootNode.getAllChildNodes();

        if (childrenOfRoot.size() == 0) {
            throw new IllegalStateException("The root node has no children!");
        } else if (childrenOfRoot.size() == 1) {
            return childrenOfRoot.iterator().next().getMove();
        }

        Node bestNode = null;
        int maxValue = MIN_SCORE;
        for (Node child : currentRootNode.getAllChildNodes()) {
            child.setMinMaxValue(minmax(child, currentTreeDepth, MIN_SCORE, MAX_SCORE, true));

            if (maxValue < child.getMinMaxValue()) {
                bestNode = child;
                Debug.print("updated node: " + child.getMove());
                Debug.print("updated minMaxValue: " + maxValue);
                maxValue = bestNode.getMinMaxValue();
            }
        }

        Debug.print("Best score Node Value: " + bestNode.getMinMaxValue());

        return bestNode.getMove();
    }

    /**
     * Minmax algorithm combined with a branch and bound method (alpha-beta pruning). <br>
     * @param node The node to calculate the minMax value for.
     * @param depth The tree depth we want to investigate.
     * @param alpha Initial lower bound.
     * @param beta Initial upper bound.
     * @param maxPlayer True if current node is a maxPlayer (AI) turn.
     * @return Minmax value of that node.
     */
    public int minmax(Node node, int depth, int alpha, int beta, boolean maxPlayer) {
        if (depth == 0 || node.getAllChildNodes().size() == 0) {
            return node.getPointDifference();
        }
        int bestValue;
        if (maxPlayer) {
            bestValue = MIN_SCORE;
            for (Node child : node.getAllChildNodes()) {
                bestValue = Math.max(bestValue, minmax(child, depth - 1, alpha, beta, false));
                Debug.print("(MAX) current minMaxValue: " + bestValue);
                alpha = Math.max(alpha, bestValue);
                if (alpha >= beta) {
                    break;  // pruning
                }
            }
            return bestValue;
        } else {
            bestValue = MAX_SCORE;
            for (Node child : node.getAllChildNodes()) {
                bestValue = Math.min(bestValue, minmax(child, depth - 1, alpha, beta, true));
                Debug.print("(MIN) current minMaxValue: " + bestValue);
                beta = Math.min(beta, bestValue);
                if (alpha >= beta) {
                    break;  // pruning
                }
            }
            return bestValue;
        }
    }

    /**
     * Sets the current tree depth and width
     * depending on the current amount of possible moves.
     */
    private void setTreeSize() {
        if (myBoard.getPossibleMoves().size() >= MAX_MOVES_SIZE_FOR_TREE) {
            currentTreeDepth = 1;
            currentTreeWidth = 1;
        } else if (myBoard.getPossibleMoves().size() >= MAX_MOVES_SIZE_FOR_TREE / 2) {
            currentTreeDepth = 3;
            currentTreeWidth = 2;
        } else {
            currentTreeDepth = 5;
            currentTreeWidth = 3;
        }
    }
}
