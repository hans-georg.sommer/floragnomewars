package flowerwarspp.player;

/**
* Enum for the different states that the player objects can have
* @version 1
* @author Felix Strnad
*/
public enum PlayerStatus {
    NOTEXISTING,
    INITIALIZED,
    REQUEST,
    CONFIRM,
    UPDATE
}
