package flowerwarspp.player;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import flowerwarspp.preset.Ditch;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Player;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Status;

/**
 * This class implements a Network player that uses RMI.
 * @author Felix Strnad
 */
public class NetworkPlayer extends UnicastRemoteObject implements Player {
    /** Wrapped player object. */
    private final Player player;

    /**
     * Create a network wrapper for the given player.
     * @param player Player which is used by the network.
     * @throws RemoteException if RMI connection failed.
     */
    public NetworkPlayer(Player player) throws RemoteException {
        this.player = player;
    }

    /*
     * @see flowerwarspp.preset.Player#init(int, flowerwarspp.preset.PlayerColor)
     */
    @Override
    public void init(int size, PlayerColor color) throws Exception, RemoteException {
        player.init(size, color);
    }

    /**
     * Sanitize a move to make sure, that both the static and the dynamic type
     * are preset types.
     * @param move Move to sanitize.
     * @return The sanitized move.
     */
    public static Move sanitize(Move move) {
        switch (move.getType()) {
        case Flower:
            Flower first = move.getFirstFlower();
            Flower second = move.getSecondFlower();
            return new Move(new Flower(first.getFirst(), first.getSecond(), first.getThird()),
                            new Flower(second.getFirst(), second.getSecond(), second.getThird()));
        case Ditch:
            Ditch ditch = move.getDitch();
            return new Move(new Ditch(ditch.getFirst(), ditch.getSecond()));
        default:
            return move;
        }
    }

    /*
     * @see flowerwarspp.preset.Player#request()
     */
    @Override
    public Move request() throws Exception, RemoteException {
        return sanitize(player.request());
    }

    /*
     * @see flowerwarspp.preset.Player#confirm(flowerwarspp.preset.Status)
     */
    @Override
    public void confirm(Status boardStatus) throws Exception, RemoteException {
        player.confirm(boardStatus);
    }

    /*
     * @see flowerwarspp.preset.Player#update(flowerwarspp.preset.Move, flowerwarspp.preset.Status)
     */
    @Override
    public void update(Move opponentMove, Status boardStatus) throws Exception, RemoteException {
        player.update(opponentMove, boardStatus);
    }
}
