/**
 * Contains the different players, e.g. the human player, random player, ...
 */
package flowerwarspp.player;