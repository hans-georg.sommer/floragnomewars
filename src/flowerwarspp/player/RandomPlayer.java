package flowerwarspp.player;

import java.util.Random;

import flowerwarspp.Debug;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveType;
import flowerwarspp.preset.PlayerType;

/**
 * This is the implementation of a Random player. He / She takes a draw just based on a random choice of all
 * possible draws without any preference.
 * @author Felix Strnad
 * @version 1
 *
 */
public class RandomPlayer extends BasicPlayer {
    /** Random generator we use to choose a random move. */
    private final Random random = new Random();

    /**
     * Default (and only) constructor for the random.
     */
    public RandomPlayer() {
        this.type = PlayerType.RANDOM_AI;
    }

    /**
     * Requests a new draw of the random player
     * @return Move for the next move.
     * @throws Exception if return move is null.
     */
    @Override
    public Move chooseMove() throws Exception {
        Debug.print("Choose Random Move! " + status + " " + color);

        // Using the Random class of java we can choose randomly one possible move for the random player.
        Move[] possibleMoves = myBoard.getPossibleMoves().toArray(new Move[0]);
        int sizePossibleDraws = possibleMoves.length;

        Move randomMove = null;
        do {
            randomMove = possibleMoves[random.nextInt(sizePossibleDraws)];
            if (randomMove == null) {
                throw new Exception("The move the Random player wants to make is null!");
            }
        } while (randomMove.getType() == MoveType.End || randomMove.getType() == MoveType.Surrender);

        return randomMove;
    }

}
