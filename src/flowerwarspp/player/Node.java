package flowerwarspp.player;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;

import flowerwarspp.Debug;
import flowerwarspp.boardmechanics.ColoredDitch;
import flowerwarspp.boardmechanics.ColoredFlower;
import flowerwarspp.boardmechanics.FGWBoard;
import flowerwarspp.boardmechanics.FGWType;
import flowerwarspp.boardmechanics.FlowerSet;
import flowerwarspp.preset.Flower;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.MoveType;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Position;
import visualtree.DrawableTreeElement;

/**
 * Node to evaluate different moves of the IntelligentPlayer.
 * @author Felix Strnad, (Martin Heide)
 *
 */
public class Node implements DrawableTreeElement {
    /** Size how many ditches are considered to be good moves. */
    private static final int MAX_SIZE_OF_DITCH_MOVES = 2;
    /** Maximum scoring for one possible node. */
    private static final int MAX_SCORE = 1000000;
    /** Minimum scoring for one possible node, not choose this node. */
    private static final int MIN_SCORE = -1000000;
    /** To differ between large and small boards. */
    private static final int SMALL_BOARD = 6;
    /** Score if this ditches connects Flower sets with 4 and 3 flowers. */
    private static final int DITCH_CONNECT_4_3 = 47;
    /** Score if this ditches connects Flower sets with 3 and 3 flowers. */
    private static final int DITCH_CONNECT_3_3 = 44;
    /** Score if both flowers are edge neighbors. */
    private static final int EDGE_NEIGHBOR_SCORE = 25;
    /** Score if this node is in the outer neighborhood. */
    private static final int NODE_IN_OUTER_NEIGHBORHOOD = 20;
    /** Move of this node. */
    private Move move;
    /** Value of this node, according to minmax-algorithm. */
    private int minMaxValue;
    /** Point difference between opponent and own points of this node.*/
    private int pointDifference = 0;
    /** All child nodes of this node.*/
    private HashMap<Move, Node> childNodes;
    /** Board of this node. */
    private FGWBoard myBoard;
    /** Color of the KI player. */
    private PlayerColor AIcolor;
    /** Color of the player's turn on this node */
    private PlayerColor nodeColor;

    // Collections for chooseChildNodes
    /** Hashmap of all possible flowers on this node.*/
    private HashMap<Integer, ColoredFlower> possibleFlower;
    /** Hashmap of all possible ditches on this node. */
    private HashMap<Integer, ColoredDitch> possibleDitches;
    /** Collection of all existing on this node. */
    private Collection<FlowerSet> existingFlowerSets;
    /** Collection of all possible Moves that are flower moves on this node. */
    private Collection<Move> possibleMovesFlower;
    /** Collection of all possible ditch moves on this node. */
    private Collection<Move> possibleMovesDitch;

    /** Indicates the maximal amount of children/ditch moves in the chooseAndScore function. */
    private final int treewidth;

    /**
     * Constructor for the root node.
     * @param board Reference to the board of the player.
     * @param color Color of the AI.
     * @param treewidth The width of the current tree.
     */
    public Node(FGWBoard board, PlayerColor color, int treewidth) {
        move = new Move(MoveType.Surrender);
        myBoard = board;
        nodeColor = myBoard.getCurrentPlayerColor();
        AIcolor = color;
        this.treewidth = treewidth;
        calculatePointDifference();
        childNodes = new HashMap<Move, Node>();
    }

    /**
     * Constructor for the "child" nodes.
     * @param move The move of the child node.
     * @param parentNode The parent node.
     * @param color Color of the AI.
     * @param board Reference to the board of the player.
     * @param treewidth The width of the tree.
     */
    public Node(Move move, Node parentNode, PlayerColor color, FGWBoard board, int treewidth) {
        myBoard = board;
        nodeColor = myBoard.getCurrentPlayerColor();
        AIcolor = color;
        this.treewidth = treewidth;
        childNodes = new HashMap<Move, Node>();
        this.move = move;
    }

    /**
     * Returns all children of the node. For Red Blue Tree drawer in debug mode.
     */
    @Override
    public Collection<DrawableTreeElement> getChildren() {
        Collection<DrawableTreeElement> col = new HashSet<DrawableTreeElement>();

        for (Node n : childNodes.values()) {
            col.add(n);
        }

        return col;
    }

    /**
     * Checks whether the node is red or not.
     */
    @Override
    public boolean isRed() {
        return (nodeColor == PlayerColor.Red);
    }

    /**
     * Returns the move that belongs to this node.
     * @return Move of this node.
     */
    @Override
    public Move getValue() {
        return getMove();
    }


    /**
     * Returns the current point difference between the two players at the gamestate of this node.
     * @return Point difference.
     */
    public int getPointDifference() {
        return pointDifference;
    }

    /**
     * Sets the minMax value of this node.
     * @param value The minMax value.
     */
    public void setMinMaxValue(int value) {
        minMaxValue = value;
    }

    /**
     * Returns the minMaxValue of this node.
     * @return The minMax value.
     */
    public int getMinMaxValue() {
        return minMaxValue;
    }

    /**
     * Returns all child nodes of this node.
     * @return Collection of all child nodes.
     */
    public Collection<Node> getAllChildNodes() {
        return childNodes.values();
    }

    /**
     * Generates all child nodes to this parent node.
     * @param moves Collection of moves.
     */
    public void setChildNodes(Collection<Move> moves) {
        for (Move m : moves) {
            Node n = new Node(m, this, AIcolor, myBoard, treewidth);
            childNodes.put(m,n);
        }
    }

    /**
     * Generates one single child, if only one move is considered.
     * @param move The move for this child.
     */
    public void setChildNodes(Move move) {
        childNodes.put(move,new Node(move, this, AIcolor, myBoard, treewidth));
    }

    /**
     * Returns the move of the node.
     * @return The move of that node.
     */
    public Move getMove() {
        if (move == null) {
            throw new IllegalArgumentException("getMove: This Node hasnt obtained a Move yet!!");
        }
        return move;
    }

    /**
     * Calculates the corresponding FGWType for the current player color.
     * @return RED or BLUE in types of which the flowers are.
     */
    public FGWType computeFGWType() {
        if (myBoard.getCurrentPlayerColor() == PlayerColor.Red) {
            return FGWType.RED;
        } else {
            return FGWType.BLUE;
        }
    }

    /**
     * Calculates the point difference for this node / state with the opponent's points.
     */
    public void calculatePointDifference() {
        if (AIcolor == PlayerColor.Red) {
            pointDifference = myBoard.getPoints(PlayerColor.Red) - myBoard.getPoints(PlayerColor.Blue);
        } else {
            pointDifference = myBoard.getPoints(PlayerColor.Blue) - myBoard.getPoints(PlayerColor.Red);
        }
        if (move != null) {
            if (move.getType() == MoveType.End && pointDifference > 0) {
                pointDifference = MAX_SCORE;
            } else if (move.getType() == MoveType.End && pointDifference < 0) {
                pointDifference = MIN_SCORE;
            }
        }
    }


    /**
     * This function is used to score the ChildNodes of one rootNode.
     * @param treedepth The depth of the tree.
     */
    public void buildTree(int treedepth) {
        chooseChildNodes();

        if (getAllChildNodes().size() == 1) {
            getAllChildNodes().iterator().next().calculatePointDifference();
            return;
        }
        // if number of childnodes == 1, return here!
        for (Node n : getAllChildNodes()) {
            n.valueTheNode(treedepth - 1);
        }
    }

    /**
     * Score one specific node, based on the different scoring algorithms.
     * @param treedepth Depth of the tree.
     */
    private void valueTheNode(int treedepth) {
        // iff move != End Move
        myBoard.makeTree(move);
        calculatePointDifference();
        Debug.print("depth: " + treedepth);
        Debug.print("pointDifference: " + pointDifference);
        if (treedepth > 0) {
            buildTree(treedepth);
        }
        myBoard.unmakeTree(move);
        return;
    }

    /**
     *  Chooses the children for this node. This is done in a sophisticated way to choose only this possibles moves out of
     *  all possible moves that most probable will score more points. At the end this will be set to the new child nodes.
     */
    private void chooseChildNodes() {

        updatePossibleLists();

        // Case: End Move!
        if (possibleMovesFlower.size() == 0 && getPointDifference() > 0) {
            setChildNodes(new Move(MoveType.End));
            return;
        }
        // Case: No flowers of my color exist, i.e. at the beginning of the game!
        if (existingFlowerSets.isEmpty()) {
            firstMove(possibleFlower, possibleMovesFlower);
            return;
        }

        // Case: FlowerSets of my color exist!
        HashMap<Move, Integer> scoredMoves = new HashMap<Move, Integer>();
        HashMap<Move, Integer> scoredDitchMoves = new HashMap<Move, Integer>();

        for (Move move : possibleMovesDitch) {
            scoredDitchMoves.put(move, scoreDitchMove(move, possibleFlower));
        }

        if (scoredDitchMoves.size() > 2) {
            scoredMoves.putAll(chooseBestDitchMoves(scoredDitchMoves));
        } else {
            scoredMoves.putAll(scoredDitchMoves);
        }

        // Run through flowers which are edge neighbors of flowersets of my color and which are possible.
        HashSet<ColoredFlower> allColoredFlowers = new HashSet<ColoredFlower>();
        HashSet<ColoredFlower> allNodeNeighbors = new HashSet<ColoredFlower>();
        HashSet<ColoredFlower> nodeNeighbors = new HashSet<ColoredFlower>();
        HashSet<ColoredFlower> edgeNeighbors = new HashSet<ColoredFlower>();
        HashSet<ColoredFlower> edgeWithoutNodeNeighbors = new HashSet<ColoredFlower>();

        computeDirectNeighborhood(allColoredFlowers, allNodeNeighbors, nodeNeighbors, edgeNeighbors, edgeWithoutNodeNeighbors);

        // Now we search for good Flower Moves.
        HashMap<Move, Integer> goodFlowerMoves;
        // Score Flowers which are edges but no neighbors
        HashMap<ColoredFlower, Integer> scoredFlowers = new HashMap<ColoredFlower, Integer>();
        int sizeEdgeWithoutNodeNeighors = edgeWithoutNodeNeighbors.size();

        // Search in direct Neighborhood for suitable Flowers.
        for (ColoredFlower cf : edgeWithoutNodeNeighbors) {
            if (sizeEdgeWithoutNodeNeighors > 1) {
                scoredFlowers.put(cf, MAX_SCORE);  // If we can fill a garden, this move has to best ranked.
            }
        }
        goodFlowerMoves = transformColoredFlowersToMoves(scoredFlowers, possibleMovesFlower, "");

        if (goodFlowerMoves.size() > 0) {
            scoredMoves.putAll(goodFlowerMoves);
        } else if (goodFlowerMoves.size() == 0) {
            // case consider neighborhoods of neighborhoods of flowerSets
            scoredFlowers = new HashMap<ColoredFlower, Integer>();

            // Here we identify the flowers that are in the next possible neighborhood and score them.
            scoredFlowers = computeOuterNeighborhood(allNodeNeighbors, allColoredFlowers, edgeNeighbors, edgeWithoutNodeNeighbors, possibleFlower);

            // Now all this single (!) scored Flowers of the second case will be mapped to possible flower moves.
            goodFlowerMoves = transformColoredFlowersToMoves(scoredFlowers, possibleMovesFlower,"outerNeighborhood");
            if (goodFlowerMoves.size() > 0) {
                scoredMoves.putAll(goodFlowerMoves);
            }
        }
        // If we are here, we use an educated guess for a still (paired) flower move.
        if (scoredMoves.isEmpty() && possibleMovesFlower.size() > 0) {
            computeEducatedGuess(scoredMoves, possibleFlower, possibleMovesFlower);
        }
        setChildNodes(chooseMostPromisingMoves(scoredMoves));
        return;
    }

    /**
     * The list of possible flowers, possible ditches and existing flower sets and furthermore possible moves
     * is updated for each node, since they will change according to the underlying move of the node.
     */
    private void updatePossibleLists() {
        if (myBoard.getCurrentPlayerColor() == PlayerColor.Red) {
            possibleFlower = myBoard.getRedPossibleFlowers();
            possibleDitches = myBoard.getRedPossibleDitches();
            existingFlowerSets = myBoard.getRedFlowers();
        } else {
            possibleFlower = myBoard.getBluePossibleFlowers();
            possibleDitches = myBoard.getBluePossibleDitches();
            existingFlowerSets = myBoard.getBlueFlowers();
        }
        possibleMovesFlower = myBoard.getPossibleMovesFlower();
        possibleMovesDitch = myBoard.getPossibleMovesDitch();
    }

    /**
     * This function computes one (!) node for the first move. Depending on the boardsize, we return set a collection of nodes
     * or just a single move.
     * @param possibleFlower List of all possible flowers on this node.
     * @param possibleMoveFlower List of all possible moves that are flower moves.
     */
    private void firstMove(HashMap<Integer, ColoredFlower> possibleFlower, Collection<Move> possibleMoveFlower) throws NullPointerException {

        HashSet<Move> pairedMoves = new HashSet<Move>();
        int boardSize = myBoard.getBoardSize();
        if (boardSize < SMALL_BOARD) {
            for (Move move : possibleMovesFlower) {
                Flower firstFlower = move.getFirstFlower();
                Flower secondFlower = move.getSecondFlower();
                if (isAtBorder(firstFlower, boardSize) && isAtBorder(secondFlower, boardSize)) {
                    if (myBoard.getColoredFlower(firstFlower).isEdgeNeighbor(myBoard.getColoredFlower(secondFlower))) {
                        pairedMoves.add(move);
                    }
                }
            }
            setChildNodes(pairedMoves);
            return;
        } else {
            // Set First Move in the middle of the board.
            int halfBoardSize = myBoard.getBoardSize() / 2;
            Flower startFlower = null;
            for (int index = 0; index < 3; ++index) {
                startFlower = new Flower(new Position(halfBoardSize,halfBoardSize + index),
                        new Position(halfBoardSize,halfBoardSize + 1 + index), new Position(halfBoardSize + 1, halfBoardSize + index));
                if (possibleFlower.containsKey(startFlower.hashCode())) {
                    break;
                }
                if (halfBoardSize + index > myBoard.getBoardSize()) {
                    startFlower = possibleFlower.values().iterator().next();
                }
            }
            Move move = null;
            for (ColoredFlower cf : possibleFlower.get(startFlower.hashCode()).getFlowerEdgeNeighbors()) {
                move = new Move(startFlower, cf);
                if (possibleMoveFlower.contains(move)) {
                    setChildNodes(move);
                    return;
                }
            }
            if (!(possibleMoveFlower.contains(move))) {
                Debug.print("Random First move!");
                move = possibleMoveFlower.iterator().next();
                setChildNodes(move);;
            }
            if (move == null) {
                throw new NullPointerException("Null pointer in First Move of game!");
            }
        }
        return;
    }

    /**
     * Checks whether a flower is at the border of the game or not.
     * @param flower Flower of which we want know if it is at border.
     * @param boardSize Size of the board
     * @return boolean if at Border or not.
     */
    private boolean isAtBorder (Flower flower, int boardSize) {
        boolean atBorder = false;
        Position[] flowerPosition = new Position[3];
        flowerPosition[0] = flower.getFirst();
        flowerPosition[1] = flower.getSecond();
        flowerPosition[2] = flower.getThird();
        for (int i = 0; i < 3; i++) {
            if (flowerPosition[i].getColumn() == 1 || flowerPosition[i].getRow() == 1
                    || flowerPosition[i].getColumn() + flowerPosition[i].getRow() == boardSize + 1) {
                atBorder = true;
            }
        }
        return atBorder;
    }



    /**
     * This functions scores the different possible DitchMoves.
     * @param move Move which needs to be scored.
     * @param possibleFlower HashMap of all possible Flowers
     * @return Integer that scores the ditch move.
     */
    private int scoreDitchMove(Move move, HashMap<Integer, ColoredFlower> possibleFlower) {
        if (move.getType() != MoveType.Ditch) {
            throw new IllegalArgumentException("No Ditch move in scoreDitchMove!");
        }
        ColoredDitch cditch = myBoard.getColoredDitch(move.getDitch());
        Collection<ColoredFlower> nodeNeighbors1 = cditch.getFlowerNodeNeighborsOne();
        Collection<ColoredFlower> nodeNeighbors2 = cditch.getFlowerNodeNeighborsTwo();

        HashSet<FlowerSet> allFlowerSets1 = new HashSet<FlowerSet> ();
        for (ColoredFlower cf : nodeNeighbors1) {
            if (cf.getFlowerType() == computeFGWType()) {
                allFlowerSets1.add(cf.getParentFlowerSet());
            }
        }

        HashSet<FlowerSet> allFlowerSets2 = new HashSet<FlowerSet> ();
        for (ColoredFlower cf : nodeNeighbors2) {
            if (cf.getFlowerType() == computeFGWType()) {
                allFlowerSets2.add(cf.getParentFlowerSet());
            }
        }
        if (allFlowerSets1.size() > 1 || allFlowerSets2.size() > 1) {
            Debug.print(move.toString() +  " Sets" + allFlowerSets1.size() + " " + allFlowerSets2.size());
            return -100;
        }
        if (allFlowerSets1.size() == 0 || allFlowerSets2.size() == 0) {
            Debug.print("Ditch has no neighboring flowersets");
            throw new IllegalStateException("Ditch has no neighboring flowersets");
        }
        FlowerSet flowerSet1 = allFlowerSets1.iterator().next();
        FlowerSet flowerSet2 = allFlowerSets2.iterator().next();


        // The two Flower Sets are already connected by 1 ditch
        if (flowerSet1.isConnected(flowerSet2, null)) {
            return MIN_SCORE;
        }

        int sizeFS1 = flowerSet1.getSize();
        int sizeFS2 = flowerSet2.getSize();
        if (sizeFS1 == 4 && sizeFS2 == 4) {
            return MAX_SCORE;
        } else if (sizeFS1 == 4 || sizeFS2 == 4) {
            FlowerSet unfullFlowerSet;
            if (sizeFS1 == 4) {
                unfullFlowerSet = allFlowerSets2.iterator().next();
            } else {
                unfullFlowerSet = allFlowerSets1.iterator().next();
            }
            if (unfullFlowerSet.getSize() >= 2) {
                int numPossibleFlowers = 0;
                for (ColoredFlower cf : unfullFlowerSet.getFlowerEdgeNeighbors()) {
                    if (possibleFlower.containsValue(cf)) {
                        numPossibleFlowers += 1;
                    }
                }
                return DITCH_CONNECT_4_3 + numPossibleFlowers;
            }
        } else if (sizeFS1 == 3 && sizeFS2 == 3) {
            int numPossibleFlowers = 0;
            for (ColoredFlower cf : allFlowerSets1.iterator().next().getFlowerEdgeNeighbors()) {
                if (possibleFlower.containsValue(cf)) {
                    numPossibleFlowers += 1;
                }
            }
            for (ColoredFlower cf : allFlowerSets2.iterator().next().getFlowerEdgeNeighbors()) {
                if (possibleFlower.containsValue(cf)) {
                    numPossibleFlowers += 1;
                }
            }
            return DITCH_CONNECT_3_3 + numPossibleFlowers;
        }
        return 0;
    }


    /**
     * This function chooses from all possible ditch moves the best MAX_SIZE_OF_DITCH_MOVES Moves.
     * @param scoredDitchMoves Set that contains all scoredDitchMoves.
     * @return Best scored ditch moves.
     */
    private HashMap<Move, Integer> chooseBestDitchMoves(HashMap<Move, Integer> scoredDitchMoves) {
        HashMap<Move, Integer> mostPromisingDitchMoves = new HashMap<Move, Integer>();
        for (int index = 0; index < MAX_SIZE_OF_DITCH_MOVES; index++) {
            int bestScoring = MIN_SCORE;
            Move bestMove = null;
            for (Move sM : scoredDitchMoves.keySet()) {
                if (scoredDitchMoves.get(sM) >= bestScoring) {
                    bestMove = sM;
                    bestScoring = scoredDitchMoves.get(sM);
                }
            }
            if (myBoard.getBoardSize() >= SMALL_BOARD) {
                mostPromisingDitchMoves.put(bestMove, bestScoring);
            } else {
                if (bestScoring == MAX_SCORE || possibleMovesFlower.size() == 0) {
                    // possibleFlower.size() because in case pointDifference< 0 and Endmove possible we need to do this move!
                    mostPromisingDitchMoves.put(bestMove, bestScoring);
                }
            }
            scoredDitchMoves.remove(bestMove);
        }
        return mostPromisingDitchMoves;
    }

    /**
     * The direct neighbors of already existing FlowerSets are computed here.
     * @param allColoredFlowers Contains all colored flowers.
     * @param allNodeNeighbors Contains all node neighbors.
     * @param nodeNeighbors Contains node neighbors of the existing flower sets.
     * @param edgeNeighbors Contains edge neighbors of the existing flower sets.
     * @param edgeWithoutNodeNeighbors Contains the set of edges without nodeneighbors of all existing flower sets.
     */
    private void computeDirectNeighborhood(HashSet<ColoredFlower> allColoredFlowers,HashSet<ColoredFlower> allNodeNeighbors,
            HashSet<ColoredFlower> nodeNeighbors, HashSet<ColoredFlower> edgeNeighbors, HashSet<ColoredFlower> edgeWithoutNodeNeighbors) {

        for (FlowerSet fs : existingFlowerSets) {
            allColoredFlowers.addAll(fs.getFlowers());
            for (ColoredFlower cf : fs.getFlowerNodeNeighbors()) {
                allNodeNeighbors.add(cf);
                if (possibleFlower.containsKey(cf.hashCode())) {
                    nodeNeighbors.add(cf);
                }
            }
            for (ColoredFlower cf : fs.getFlowerEdgeNeighbors()) {
                if (possibleFlower.containsKey(cf.hashCode())) {
                    edgeNeighbors.add(cf);
                    // consider only the edgeNeighbors which are not included in the node neighborhood!
                    if (!nodeNeighbors.contains(cf)) {
                        edgeWithoutNodeNeighbors.add(cf);
                    }
                }
            }
        }
        return;
    }

    /**
     * Computes the outer neighborhood of an already existing FlowerSet.
     * @param allNodeNeighbors Contains all node neighbors.
     * @param allColoredFlowers Contains all colored flowers.
     * @param edgeNeighbors Contains edge neighbors of the existing FlowerSet.
     * @param nodeNeighbors Contains node neighbors of the existing FlowerSet.
     * @param possibleFlower Contains all possible flowers of this node.
     * @return HashMap of the outerNeighborhood of an FlowerSet.
     */
    private HashMap<ColoredFlower, Integer> computeOuterNeighborhood(HashSet<ColoredFlower> allNodeNeighbors,
            HashSet<ColoredFlower> allColoredFlowers,  HashSet<ColoredFlower> edgeNeighbors, HashSet<ColoredFlower> nodeNeighbors,
            HashMap<Integer, ColoredFlower> possibleFlower) {

        HashMap<ColoredFlower, Integer> scoredFlowers = new HashMap<ColoredFlower, Integer>();

        for (ColoredFlower innerCF : allNodeNeighbors) {
            Vector<ColoredFlower> OuterNeighborhood = new Vector<ColoredFlower>();
            Vector<ColoredFlower> dummyset = new Vector<ColoredFlower>();
            dummyset.addAll(innerCF.getFlowerEdgeNeighbors());
            dummyset.addAll(innerCF.getFlowerNodeNeighbors());

            // dummyset are all surrounding flowers of the direct (!) edge/node neighborhood
            for (ColoredFlower outerNeighborFlower : dummyset) {
                if (!allColoredFlowers.contains(outerNeighborFlower)
                        && !allNodeNeighbors.contains(outerNeighborFlower)
                        && !edgeNeighbors.contains(outerNeighborFlower)
                        && possibleFlower.containsValue(outerNeighborFlower)) {
                    OuterNeighborhood.add(outerNeighborFlower);
                }
            }
            for (ColoredFlower neighbor : OuterNeighborhood) {
                if (!nodeNeighbors.contains(neighbor) && neighbor.getFlowerType() == FGWType.UNCOLORED
                        && possibleFlower.containsValue(neighbor)) {
                    int numberOfUncoloredEdgeNeighbors = 0;
                    for (ColoredFlower cf : neighbor.getFlowerEdgeNeighbors()) {
                        if (cf.getFlowerType() == FGWType.UNCOLORED) {
                            numberOfUncoloredEdgeNeighbors += 1;
                        }
                    }
                    if (numberOfUncoloredEdgeNeighbors > 1) {
                        scoredFlowers.put(neighbor,scoreFlower(neighbor));  // Here we really score the flower moves!
                    }
                }
            }
        }
        return scoredFlowers;
    }

    /**
     * Edge neighbors of neighboring flowers will obtain a higher scoring.
     * @param bestFlower Flower that was best scored before.
     * @param scoredFlowers Set of all scored Flowers.
     * @return HashMap of outerNeighborhood.
     */
    private HashMap<ColoredFlower, Integer> outerNeighborhoodScoring(ColoredFlower bestFlower, HashMap<ColoredFlower, Integer> scoredFlowers) {
        for (ColoredFlower cf : bestFlower.getFlowerEdgeNeighbors()) {
            if (scoredFlowers.containsKey(cf)) {
                scoredFlowers.put(cf, scoredFlowers.get(cf) + EDGE_NEIGHBOR_SCORE);
            }
        }
        return scoredFlowers;
    }

    /**
     * Score the value of a flower that can be possibly set in the second case of setting a flower.
     * @param flower Colored flower which will be scored.
     * @return Integer score of the flower.
     */
    private int scoreFlower(ColoredFlower flower) {
        Collection<ColoredFlower> edgeNeighbors = flower.getFlowerEdgeNeighbors();
        int fSize = 0;
        for (ColoredFlower cf : edgeNeighbors) {
            cf.getParentFlowerSet();

            if (!(cf.getParentFlowerSet() == null) && cf.getFlowerType() == computeFGWType()) {
                fSize = cf.getParentFlowerSet().getSize();
            }
        }

        if (fSize == 3) {
            return 50;
        } else if (fSize == 2) {
            return 30;
        } else if (fSize == 1) {
            return 25;
        }
        return NODE_IN_OUTER_NEIGHBORHOOD;
    }

    /**
     * Transforms all the (single)scored flowers into the best combinations of moves.
     * @param scoredFlowers Set of all scored flowers.
     * @param possibleMoveFlower Set of all moves that are Flower moves.
     * @param caseScoring Differences if we transform flowers that belong to direct and outer neighborhood.
     * @return HashMap of all scored FlowerMoves to an value.
     */
    private HashMap<Move, Integer> transformColoredFlowersToMoves(HashMap<ColoredFlower, Integer> scoredFlowers, Collection<Move> possibleMoveFlower, String caseScoring) {

        int firstFlowerScore;
        HashMap<Move, Integer> goodFlowerMoves = new HashMap<Move, Integer>();
        while (goodFlowerMoves.size() < treewidth && scoredFlowers.size() > 1) {
            ColoredFlower bestFlower = null;
            ColoredFlower secondBestFlower = null;
            int bestScoring = MIN_SCORE;
            for (ColoredFlower cf : scoredFlowers.keySet()) {
                if (scoredFlowers.get(cf) > bestScoring) {
                    bestFlower = cf;
                    bestScoring = scoredFlowers.get(cf);
                }
            }
            firstFlowerScore = scoredFlowers.get(bestFlower);
            scoredFlowers.remove(bestFlower);

            Move testMove = null;
            HashMap<ColoredFlower, Integer> testScoredFlowers = new HashMap<ColoredFlower, Integer>();

            if (caseScoring.equals("outerNeighborhood")) {
                testScoredFlowers.putAll(outerNeighborhoodScoring(bestFlower, scoredFlowers));
            } else {
                testScoredFlowers.putAll(scoredFlowers);
            }
            Collection<ColoredFlower> edgeNeigboursBestFlower = bestFlower.getFlowerEdgeNeighbors();
            int fSize = 0;
            for (ColoredFlower cf : edgeNeigboursBestFlower) {
                cf.getParentFlowerSet();
                if (!(cf.getParentFlowerSet() == null) && cf.getFlowerType() == computeFGWType()) {
                    fSize = cf.getParentFlowerSet().getSize();
                }
            }
            // Check if Edge Neighbor to Flower can finish the flowerset!
            for (ColoredFlower cf : bestFlower.getFlowerEdgeNeighbors()) {
                if (fSize > 1 && cf.getFlowerType() == FGWType.UNCOLORED) {
                    secondBestFlower = cf;
                    testMove = new Move (bestFlower, secondBestFlower);

                    if (possibleMoveFlower.contains(testMove)) {
                        goodFlowerMoves.put(testMove, firstFlowerScore + MAX_SCORE);
                    }
                } else { // Or, be at least edgeNeighbor in outerNeighborhood case.
                    testMove = new Move(bestFlower, cf);
                    if (possibleMoveFlower.contains(testMove)) {
                        // Sum the single of scoring of the two flowers.
                        goodFlowerMoves.put(testMove, firstFlowerScore + EDGE_NEIGHBOR_SCORE);
                    }
                }
            }
            for (ColoredFlower cf : testScoredFlowers.keySet()) {
                if (testScoredFlowers.get(cf) == MAX_SCORE) {
                    testMove = new Move (bestFlower, cf);
                    if (possibleMoveFlower.contains(testMove)) {
                        goodFlowerMoves.put(testMove, firstFlowerScore + MAX_SCORE);
                    }
                }
            }
        }
        return goodFlowerMoves;
    }

    /**
     * In case of an empty scoredMoves HashMap, we compute moves that are scored according if they are neighbors.
     * @param scoredMoves Set of all scored moves.
     * @param possibleFlower Set of all possible flower moves.
     * @param possibleMovesFlower Contains all moves that are flower moves.
     */
    private void computeEducatedGuess(HashMap<Move, Integer> scoredMoves, HashMap<Integer, ColoredFlower> possibleFlower, Collection<Move> possibleMovesFlower) {
        HashSet<ColoredFlower> allFlowers = new HashSet<ColoredFlower>(possibleFlower.values());
        ColoredFlower firstFlower = allFlowers.iterator().next();
        allFlowers.remove(firstFlower);
        for (Move move : possibleMovesFlower) {
            if (myBoard.getColoredFlower(move.getFirstFlower()).isEdgeNeighbor(myBoard.getColoredFlower(move.getSecondFlower()))) {
                scoredMoves.put(move, EDGE_NEIGHBOR_SCORE);
            } else {
                scoredMoves.put(move, 0);
            }
        }
    }

    /**
     * Here the best scored moves from all scored moves are choosen and will then considered as possible nodes.
     * @param scoredMoves Set of all scored moves.
     * @return Collection of best scored moves.
     */
    private Collection<Move> chooseMostPromisingMoves(HashMap<Move, Integer> scoredMoves) {
        HashSet<Move> mostPromisingMoves = new HashSet<Move>();
        if (scoredMoves.size() < treewidth) {
            mostPromisingMoves.addAll(scoredMoves.keySet());
        } else {
            for (int index = 0; index < treewidth; index++) {
                int bestScoring = MIN_SCORE;
                Move bestMove = null;
                for (Move sM : scoredMoves.keySet()) {
                    //System.out.println("Move " + sM  + "Scoring "+scoredMoves.get(sM));
                    if (scoredMoves.get(sM) >= bestScoring) {
                        bestMove = sM;
                        bestScoring = scoredMoves.get(sM);
                    }
                }
                mostPromisingMoves.add(bestMove);
                scoredMoves.remove(bestMove);
            }
        }
        return mostPromisingMoves;
    }
}
