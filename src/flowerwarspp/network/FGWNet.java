package flowerwarspp.network;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import flowerwarspp.Debug;
import flowerwarspp.preset.Player;

/**
 * Provides networking features to the game using RMI.
 *
 * @author Hans-Georg Sommer
 */
public class FGWNet {
    /** Host for the registration and lookup of remote players. */
    private final String rmiHost;
    /** Port for the registration and lookup of remote players. */
    private final int rmiPort;

    /** RMI registry used. */
    private Registry registry;

    /**
     * Construct a new networking Configuration.
     *
     * @param host
     *        Host name/IP address. Can not be changed once set.
     * @param port
     *        Port, can not be changed once set.
     */
    public FGWNet(String host, int port) {
        rmiHost = host;
        rmiPort = port;
    }

    /**
     * Bind a local player instance to the RMI registry.
     *
     * The bound player instance can be found by a remote game using the
     * {@link FGWNet#find} method.
     *
     * @param player
     *        Player to be advertised.
     * @param name
     *        Name used for binding at the registry.
     *
     * @throws RemoteException
     *         On network errors.
     */
    public void offer(Player player, String name) throws RemoteException {
        System.setProperty("java.rmi.server.hostname", rmiHost);
        try {
            registry = LocateRegistry.createRegistry(rmiPort);
        } catch (RemoteException e) {
            registry = LocateRegistry.getRegistry(rmiPort);
        }
        registry.rebind(name, player);
        Debug.print(String.format("Offer player on %s:%s/%s", rmiHost, rmiPort, name));
    }

    /**
     * This method is used to request a player registered to the given name from
     * the {@link FGWNet#registry}.
     *
     * @param name
     *        Name to look up at the registry.
     *
     * @return The player object from the registry.
     */
    public Player find(String name) {
        Player player = null;
        while (player == null) {
            try {
                player = (Player) Naming.lookup(String.format("rmi://%s:%d/%s", rmiHost, rmiPort, name));
                Debug.print("Found remote player");
            } catch (Exception e) {
                Debug.print(String.format("Could not connect to %s:%s/%s", rmiHost, rmiPort, name));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    // do nothing, we are waiting for a successful connection
                }
            }
        }
        return player;
    }
}
