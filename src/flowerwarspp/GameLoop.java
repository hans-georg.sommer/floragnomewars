package flowerwarspp;

import flowerwarspp.boardmechanics.FGWBoard;
import flowerwarspp.gui.BoardDisplay;
import flowerwarspp.gui.FGWDisplay;
import flowerwarspp.player.NetworkPlayer;
import flowerwarspp.preset.Move;
import flowerwarspp.preset.Player;
import flowerwarspp.preset.PlayerColor;
import flowerwarspp.preset.Status;
import flowerwarspp.preset.Viewer;

/**
 * This class executes the main game loop in a separate thread. This makes it
 * possible to stop/restart the game at any time.
 *
 * @author Hans-Georg Sommer
 */
public class GameLoop extends Thread {
    /** Main game board on which the game takes place. */
    private final FGWBoard board;
    /** Viewer for the main game board. */
    private final Viewer viewer;
    /** Reference to the object that has to be updated after each turn. */
    private final BoardDisplay display;
    /** Reference to the top GUI object. */
    private final FGWDisplay gui;
    /** First player (starts as red). */
    private final Player firstPlayer;
    /** Second player (starts as blue). */
    private final Player secondPlayer;
    /** Rounds to play. */
    private final int totalRounds;
    /** Delay between moves. */
    private final int delay;
    /** Set to {@code true}, if the current game is finished. */
    private boolean gameOver = false;

    /**
     * Create a new GameLoop using the given objects.
     *
     * @param board
     *        The board on which the game is played.
     * @param first
     *        The first player (starts as red).
     * @param second
     *        The second player (starts as blue).
     * @param rounds
     *        The number of rounds to play.
     * @param delay
     *        Delay in ms between moves.
     * @param gui
     *        The GUI for this game.
     */
    public GameLoop(FGWBoard board, Player first, Player second, int rounds, int delay, FGWDisplay gui) {
        this.board = board;
        firstPlayer = first;
        secondPlayer = second;
        totalRounds = rounds;
        this.delay = delay;
        this.gui = gui;
        this.display = (gui != null) ? gui.getDisplay() : null;
        viewer = board.viewer();
    }

    /**
     * Check, if this game was finished.
     *
     * @return {@code true}, if the current game is finished.
     */
    public synchronized boolean isGameOver() {
        return gameOver;
    }

    /*
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        while (true) {
            try {
                execute();
            } catch (InterruptedException e) {
                Debug.print("GameLoop was interrupted. Restarting.");
            }
        }
    }

    private void execute() throws InterruptedException {
        Player currentPlayer;
        Player otherPlayer;
        int[] results = {0, 0, 0};
        int pointsFirst = 0;
        int pointsSecond = 0;
        gameOver = false;
        Move move = null;
        Status status;

        for (int round = 1; round <= totalRounds; ++round) {
            if (totalRounds > 1) {
                if (gui != null) {
                    gui.updateStatusBar(String.format("Round %d of %d", round, totalRounds));
                }  else {
                    System.err.println(String.format("Round %d of %d", round, totalRounds));
                }
            }
            status = Status.Ok;
            synchronized (this) {
                // TODO: is it possible to prevent this duplicate initialization
                // on game start?
                board.reset();
                if (display != null) {
                    display.setOddRound(round % 2 == 1);
                    display.reset();
                }
            }
            if (round % 2 == 1) {
                currentPlayer = firstPlayer;
                otherPlayer = secondPlayer;
            } else {
                currentPlayer = secondPlayer;
                otherPlayer = firstPlayer;
            }
            try {
                currentPlayer.init(viewer.getSize(), PlayerColor.Red);
                otherPlayer.init(viewer.getSize(), PlayerColor.Blue);
            } catch (java.rmi.ConnectException e) {
                System.out.println("Your peers network configuration is not suited for" +
                        "this game. See README for details.");
                System.exit(1);
            } catch (Exception e) {
                System.out.println("Could not initialize players: " + e);
                System.exit(1);
            }

            // main loop
            while (status == Status.Ok) {
                try {
                    do {
                        Debug.print("Main loop: request move from player " + viewer.getTurn()
                                  + " (" + viewer.getPossibleMoves().size() + " possible moves)");
                        move = currentPlayer.request();
                        board.make(move);
                        status = viewer.getStatus();
                    } while (status == Status.Illegal);

                    currentPlayer.confirm(status);
                    otherPlayer.update(NetworkPlayer.sanitize(move), status);
                    if (display != null) {
                        display.update(move, status);
                        Thread.sleep(delay);
                    }
                } catch (java.rmi.ConnectException e) {
                    System.out.println("Connection lost: " + e.getMessage());
                    System.exit(1);
                } catch (InterruptedException e) {
                    throw e;
                } catch (Exception e) {
                    System.out.println("Error in main loop: " + e);
                    System.exit(1);
                }

                // switch turn
                if (currentPlayer == firstPlayer) {
                    currentPlayer = secondPlayer;
                    otherPlayer = firstPlayer;
                } else {
                    currentPlayer = firstPlayer;
                    otherPlayer = secondPlayer;
                }
            } // end of main loop

            if (round % 2 == 1) {
                pointsFirst += viewer.getPoints(PlayerColor.Red);
                pointsSecond += viewer.getPoints(PlayerColor.Blue);
            } else {
                pointsFirst += viewer.getPoints(PlayerColor.Blue);
                pointsSecond += viewer.getPoints(PlayerColor.Red);
            }
            switch (status) {
            case Draw:
                Debug.print("Round " + round + " ended in a draw.");
                ++results[2];
                break;
            case RedWin:
                Debug.print("The red player won round " + round);
                ++results[(round % 2 == 1) ? 0 : 1];
                break;
            case BlueWin:
                Debug.print("The blue player won round " + round);
                ++results[(round % 2 == 0) ? 0 : 1];
                break;
            default:
                System.out.println("This should never happen, something is wrong here.");
                break;
            }
        } // end of tournament for-loop

        // print results
        System.out.println(String.format("Final result: %d:%d (%d undecided), total points: %d:%d",
                results[0], results[1], results[2], pointsFirst, pointsSecond));

        if (gui == null) {
            System.exit(0);
        } else {
            synchronized (this) {
                gameOver = true;
                wait();
            }
        }
    }
}
