package flowerwarspp;

/**
 * Small class which can be used to print debug messages depending on
 * application-wide settings.
 *
 * @author Hans-Georg Sommer
 */
public class Debug {
    /**
     * Controls, if debug messages are printed or not.
     */
    private static boolean enabled;

    /**
     * Switch debug messages on or off.
     *
     * @param bool
     *        If true, debug output will be enabled, else disabled.
     */
    public static void setEnabled(boolean bool) {
        enabled = bool;
        print("Debug output is enabled");
    }

    /**
     * Check, if debug output is enabled.
     *
     * @return {@code true}, if debug output is enabled.
     */
    public static boolean isEnabled() {
        return enabled;
    }

    /**
     * Print message to stdout, but only, if {@link Debug#enabled} is {@code true}.
     *
     * @param str
     *        The string to print.
     */
    public static void print(String str) {
        if (enabled) {
            System.out.println(str);
        }
    }
}
