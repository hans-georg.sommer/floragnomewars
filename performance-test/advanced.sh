#!/bin/bash

# executes some performance tests!

tournamentsize=1000

for i in {3..15}; do
    java -cp ../build flowerwarspp.Main -1 advanced -2 simple -s $i -t $tournamentsize --headless >> auswertung_"$i"_"$tournamentsize".txt &
done
